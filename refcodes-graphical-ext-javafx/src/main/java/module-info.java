module org.refcodes.graphical.ext.javafx {
	requires javafx.base;
	requires org.refcodes.component;
	requires transitive javafx.graphics;
	requires transitive org.refcodes.factory;
	requires transitive org.refcodes.textual;
	requires transitive org.refcodes.graphical;
	requires transitive org.refcodes.mixin;
	requires java.logging;

	exports org.refcodes.graphical.ext.javafx;
}
