package org.refcodes.graphical.ext.javafx;

import java.util.Map;

import org.refcodes.factory.ContextTypeFactory;
import org.refcodes.graphical.GridMode;
import org.refcodes.graphical.Raster;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 * A factory for creating FxRaster objects.
 */
public class FxRasterFactory implements ContextTypeFactory<Pane, Raster> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Color _evenFieldColor = Color.DARKGREEN;
	private Color _oddFieldColor = Color.LIGHTGREEN;
	private Color _fieldGapColor = Color.WHITE;

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public FxRasterFactory withEvenFieldColor( Color aEvenFieldColor ) {
		setEvenFieldColor( aEvenFieldColor );
		return this;
	}

	public FxRasterFactory withOddFieldColor( Color aOddFieldColor ) {
		setOddFieldColor( aOddFieldColor );
		return this;
	}

	public FxRasterFactory withFieldGapColor( Color aFieldGapColor ) {
		setFieldGapColor( aFieldGapColor );
		return this;
	}

	public Color getEvenFieldColor() {
		return _evenFieldColor;
	}

	public void setEvenFieldColor( Color eEvenFieldColor ) {
		_evenFieldColor = eEvenFieldColor;
	}

	public Color getOddFieldColor() {
		return _oddFieldColor;
	}

	public void setOddFieldColor( Color aOddFieldColor ) {
		_oddFieldColor = aOddFieldColor;
	}

	public Color getFieldGapColor() {
		return _fieldGapColor;
	}

	public void setFieldGapColor( Color aFieldGapColor ) {
		_fieldGapColor = aFieldGapColor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Pane create( Raster aRaster ) {
		return create( aRaster, null );
	}

	// /////////////////////////////////////////////////////////////////////////
	// FACTORIES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Pane create( Raster aRaster, Map<String, String> aProperties ) {

		Color theColor = _oddFieldColor;
		final Pane thePane = new Pane();

		int theBoardHeight = aRaster.getFieldHeight() * aRaster.getGridHeight() + ( aRaster.getFieldGap() * ( aRaster.getGridHeight() - 1 ) );
		int theBoardWidth = aRaster.getFieldWidth() * aRaster.getGridWidth() + ( aRaster.getFieldGap() * ( aRaster.getGridWidth() - 1 ) );

		int theGridOffset = 0;
		if ( aRaster.getGridMode() == GridMode.CLOSED ) {
			theBoardHeight += ( aRaster.getFieldGap() * 2 );
			theBoardWidth += ( aRaster.getFieldGap() * 2 );
			theGridOffset = aRaster.getFieldGap();
		}

		final int theBoardOffsetH = theGridOffset;
		final int theBoardOffsetV = theGridOffset;

		// Grid color:
		if ( aRaster.getFieldGap() > 0 ) {
			final Rectangle theRectangle = new Rectangle( 0, 0, theBoardWidth, theBoardHeight );
			theRectangle.setFill( _fieldGapColor );
			thePane.getChildren().add( theRectangle );
		}

		Rectangle eRect;
		int eX;
		int eY;
		for ( int y = 0; y < aRaster.getGridHeight(); y++ ) {
			for ( int x = 0; x < aRaster.getGridWidth(); x++ ) {
				eX = theBoardOffsetH + aRaster.getFieldWidth() * x + ( x * aRaster.getFieldGap() );
				eY = theBoardOffsetV + aRaster.getFieldHeight() * y + ( y * aRaster.getFieldGap() );
				eRect = new Rectangle( eX, eY, aRaster.getFieldWidth(), aRaster.getFieldHeight() );
				eRect.setFill( theColor );
				thePane.getChildren().add( eRect );
				if ( theColor.equals( _oddFieldColor ) ) {
					theColor = _evenFieldColor;
				}
				else if ( theColor.equals( _evenFieldColor ) ) {
					theColor = _oddFieldColor;
				}
			}
			theColor = y % 2 == 0 ? _evenFieldColor : _oddFieldColor;

		}

		return thePane;
	}
}
