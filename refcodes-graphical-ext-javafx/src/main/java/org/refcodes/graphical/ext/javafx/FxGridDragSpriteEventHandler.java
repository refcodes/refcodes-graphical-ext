// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical.ext.javafx;

import java.util.function.BiConsumer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.graphical.Dimension;
import org.refcodes.graphical.DragOpacityAccessor.DragOpacityBuilder;
import org.refcodes.graphical.DragOpacityAccessor.DragOpacityProperty;
import org.refcodes.graphical.FieldDimension;
import org.refcodes.graphical.FieldDimension.FieldDimensionBuilder;
import org.refcodes.graphical.FieldDimension.FieldDimensionProperty;
import org.refcodes.graphical.MoveMode;
import org.refcodes.graphical.MoveModeAccessor.MoveModeBuilder;
import org.refcodes.graphical.MoveModeAccessor.MoveModeProperty;
import org.refcodes.graphical.Offset;
import org.refcodes.graphical.Offset.OffsetBuilder;
import org.refcodes.graphical.Offset.OffsetProperty;
import org.refcodes.graphical.Opacity;
import org.refcodes.graphical.Position;
import org.refcodes.graphical.SpriteAccessor;
import org.refcodes.mixin.Disposable;

import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

/**
 * The {@link FxGridDragSpriteEventHandler} manages dragging a {@link Node}
 * inside a (virtual) grid represented by a {@link Pane} aligning that
 * {@link Node} according to the grid's field settings. A field has a width, a
 * height and a gap. The actual position of the {@link Node} on the {@link Pane}
 * is aligned to the nearest grid's position.
 */
public class FxGridDragSpriteEventHandler implements FieldDimensionProperty, FieldDimensionBuilder<FxGridDragSpriteEventHandler>, DragOpacityProperty, DragOpacityBuilder<FxGridDragSpriteEventHandler>, SpriteAccessor<Node>, MoveModeProperty, MoveModeBuilder<FxGridDragSpriteEventHandler>, OffsetProperty, OffsetBuilder<FxGridDragSpriteEventHandler>, Disposable {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( FxGridDragSpriteEventHandler.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int DEFAULT_MOVE_SPRITE_DURATION_MILLIS = 300;
	private static final int DEFAULT_DRAG_SPRITE_DURATION_MILLIS = 300;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private double _sceneX;
	private double _sceneY;
	public double _posX;
	private double _posY;
	private Node _node;
	private Pane _pane;
	private int _offsetY;
	private int _offsetX;
	private int _fieldWidth;
	private int _fieldHeight;
	private int _fieldGap;
	private double _prevOpacity = 1;
	private double _dragOpacity = Opacity.DRAG.getOpacity();
	private MoveMode _moveMode = MoveMode.SMOOTH;
	private BiConsumer<Integer, Integer> _offsetChangedListener = null;
	private BiConsumer<Integer, Integer> _mouseClickedListener = null;
	private int _moveSpriteDurationMillis = DEFAULT_MOVE_SPRITE_DURATION_MILLIS;
	private int _dragSpriteDurationMillis = DEFAULT_DRAG_SPRITE_DURATION_MILLIS;
	private boolean _isDragged;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link FxGridDragSpriteEventHandler} with the according
	 * arguments.
	 *
	 * @param aSprite The {@link Node} to be observed for drag operations and to
	 *        be aligned as of the grid's field settings.
	 * @param aPane The {@link Pane} on which the {@link Node} is to be dragged
	 *        around and onto which a (virtual) grid is projected .
	 */
	public FxGridDragSpriteEventHandler( Node aSprite, Pane aPane ) {
		this( aSprite, aPane, 0, 0, 1, 1, 0, null, null );
	}

	/**
	 * Constructs the {@link FxGridDragSpriteEventHandler} with the according
	 * arguments.
	 *
	 * @param aSprite The {@link Node} to be observed for drag operations and to
	 *        be aligned as of the grid's field settings.
	 * @param aPane The {@link Pane} on which the {@link Node} is to be dragged
	 *        around and onto which a (virtual) grid is projected .
	 * @param aOffset The initial grid {@link Offset} of the {@link Node} as of
	 *        the grid's field dimensions.
	 * @param aFieldDimension The grid's field {@link FieldDimension}.
	 */
	public FxGridDragSpriteEventHandler( Node aSprite, Pane aPane, Offset aOffset, FieldDimension aFieldDimension ) {
		this( aSprite, aPane, aOffset.getOffsetX(), aOffset.getOffsetY(), aFieldDimension.getFieldWidth(), aFieldDimension.getFieldHeight(), aFieldDimension.getFieldGap(), null, null );
	}

	/**
	 * Constructs the {@link FxGridDragSpriteEventHandler} with the according
	 * arguments.
	 *
	 * @param aSprite The {@link Node} to be observed for drag operations and to
	 *        be aligned as of the grid's field settings.
	 * @param aPane The {@link Pane} on which the {@link Node} is to be dragged
	 *        around and onto which a (virtual) grid is projected .
	 * @param aOffset The initial grid {@link Offset} of the {@link Node} as of
	 *        the grid's field dimensions.
	 * @param aFieldDimension The grid's field {@link FieldDimension}.
	 * @param aOffsetChangedListener The offset changed listener being provided
	 *        with any offset changes applied (as of dragging the scene).
	 * @param aMouseClickedListener The listener being provided with the grid
	 *        positions of where the mouse was clicked.
	 */
	public FxGridDragSpriteEventHandler( Node aSprite, Pane aPane, Offset aOffset, FieldDimension aFieldDimension, BiConsumer<Integer, Integer> aOffsetChangedListener, BiConsumer<Integer, Integer> aMouseClickedListener ) {
		this( aSprite, aPane, aOffset.getOffsetX(), aOffset.getOffsetY(), aFieldDimension.getFieldWidth(), aFieldDimension.getFieldHeight(), aFieldDimension.getFieldGap(), aOffsetChangedListener, aMouseClickedListener );
	}

	/**
	 * Constructs the {@link FxGridDragSpriteEventHandler} with the according
	 * arguments.
	 *
	 * @param aSprite The {@link Node} to be observed for drag operations and to
	 *        be aligned as of the grid's field settings.
	 * @param aPane The {@link Pane} on which the {@link Node} is to be dragged
	 *        around and onto which a (virtual) grid is projected .
	 * @param aOffsetX The initial grid X offset of the {@link Node} as of the
	 *        grid's field dimensions.
	 * @param aOffsetY The initial grid Y offset of the {@link Node} as of the
	 *        grid's field dimensions.
	 * @param aFieldWidth The grid's field width.
	 * @param aFieldHeight The grid's field height.
	 * @param aFieldGap The grid's field gap.
	 */
	public FxGridDragSpriteEventHandler( Node aSprite, Pane aPane, int aOffsetX, int aOffsetY, int aFieldWidth, int aFieldHeight, int aFieldGap ) {
		this( aSprite, aPane, aOffsetX, aOffsetY, aFieldWidth, aFieldHeight, aFieldGap, null, null );
	}

	/**
	 * Constructs the {@link FxGridDragSpriteEventHandler} with the according
	 * arguments.
	 *
	 * @param aSprite The {@link Node} to be observed for drag operations and to
	 *        be aligned as of the grid's field settings.
	 * @param aPane The {@link Pane} on which the {@link Node} is to be dragged
	 *        around and onto which a (virtual) grid is projected .
	 * @param aOffsetX The initial grid X offset of the {@link Node} as of the
	 *        grid's field dimensions.
	 * @param aOffsetY The initial grid Y offset of the {@link Node} as of the
	 *        grid's field dimensions.
	 * @param aFieldWidth The grid's field width.
	 * @param aFieldHeight The grid's field height.
	 * @param aFieldGap The grid's field gap.
	 * @param aOffsetChangedListener The offset changed listener being provided
	 *        with any offset changes applied (as of dragging the scene).
	 * @param aMouseClickedListener The listener being provided with the grid
	 *        positions of where the mouse was clicked.
	 */
	public FxGridDragSpriteEventHandler( Node aSprite, Pane aPane, int aOffsetX, int aOffsetY, int aFieldWidth, int aFieldHeight, int aFieldGap, BiConsumer<Integer, Integer> aOffsetChangedListener, BiConsumer<Integer, Integer> aMouseClickedListener ) {
		aSprite.setOnMousePressed( _onMousePressedEventHandler );
		aSprite.setOnMouseDragged( _onMouseDraggedEventHandler );
		aSprite.setOnMouseReleased( _onMouseReleasedEventHandler );
		_fieldWidth = aFieldWidth;
		_fieldHeight = aFieldHeight;
		_fieldGap = aFieldGap;
		_offsetX = aOffsetX;
		_offsetY = aOffsetY;
		_offsetChangedListener = aOffsetChangedListener;
		_mouseClickedListener = aMouseClickedListener;
		_node = aSprite;
		_pane = aPane;
		if ( _offsetX != 0 || _offsetY != 0 ) {
			_posX = _offsetX * ( _fieldWidth + _fieldGap );
			_posY = _offsetY * ( _fieldHeight + _fieldGap );
			aSprite.setTranslateX( _posX );
			aSprite.setTranslateY( _posY );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldWidth() {
		return _fieldWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldHeight() {
		return _fieldHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldGap() {
		return _fieldGap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldWidth( int aWidth ) {
		_fieldWidth = aWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withFieldWidth( int aWidth ) {
		_fieldWidth = aWidth;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldHeight( int aHeight ) {
		_fieldHeight = aHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getOffsetX() {
		return _offsetX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getOffsetY() {
		return _offsetY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOffset( int aOffsetX, int aOffsetY ) {
		final Node thePlayer = getSprite();
		final double theToX = ( getFieldWidth() + getFieldGap() ) * aOffsetX;
		final double theToY = ( getFieldWidth() + getFieldGap() ) * aOffsetY;
		thePlayer.setOpacity( 1 );
		final Runnable theRunner = () -> { final TranslateTransition theTransition = new TranslateTransition( Duration.millis( _moveSpriteDurationMillis ), thePlayer ); theTransition.setToX( theToX ); theTransition.setToY( theToY ); theTransition.setCycleCount( 1 ); theTransition.setAutoReverse( false ); theTransition.play(); theTransition.setOnFinished( evt -> { _offsetX = aOffsetX; _offsetY = aOffsetY; } ); };
		if ( Platform.isFxApplicationThread() ) {
			theRunner.run();
		}
		else {
			Platform.runLater( theRunner );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOffset( Offset aOffset ) {
		_offsetX = aOffset.getOffsetX();
		_offsetY = aOffset.getOffsetY();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOffset( Position aOffset ) {
		_offsetX = aOffset.getPositionX();
		_offsetY = aOffset.getPositionY();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOffsetX( int aOffsetX ) {
		_offsetX = aOffsetX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOffsetY( int aOffsetY ) {
		_offsetY = aOffsetY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withOffsetX( int aOffsetX ) {
		setOffsetX( aOffsetX );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withOffsetY( int aOffsetY ) {
		setOffsetY( aOffsetY );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withOffset( int aOffsetX, int aOffsetY ) {
		setOffset( aOffsetX, aOffsetY );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withOffset( Offset aOffset ) {
		setOffset( aOffset );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withOffset( Position aOffset ) {
		setOffset( aOffset );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withFieldHeight( int aHeight ) {
		_fieldHeight = aHeight;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withFieldDimension( int aWidth, int aHeight ) {
		setFieldDimension( aWidth, aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withFieldDimension( int aWidth, int aHeight, int aGap ) {
		setFieldDimension( aWidth, aHeight, aGap );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withFieldDimension( FieldDimension aDimension ) {
		setFieldDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( int aWidth, int aHeight ) {
		_fieldWidth = aWidth;
		_fieldHeight = aHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( int aWidth, int aHeight, int aGap ) {
		_fieldWidth = aWidth;
		_fieldHeight = aHeight;
		_fieldGap = aGap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( FieldDimension aDimension ) {
		_fieldWidth = aDimension.getFieldWidth();
		_fieldHeight = aDimension.getFieldHeight();
		_fieldGap = aDimension.getFieldGap();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withFieldDimension( Dimension aDimension ) {
		setFieldDimension( aDimension.getWidth(), aDimension.getHeight() );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( Dimension aDimension ) {
		_fieldWidth = aDimension.getWidth();
		_fieldHeight = aDimension.getHeight();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldGap( int aFieldGap ) {
		_fieldGap = aFieldGap;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withFieldGap( int aFieldGap ) {
		setFieldGap( aFieldGap );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getDragOpacity() {
		return _dragOpacity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDragOpacity( double aOpacity ) {
		_dragOpacity = aOpacity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withDragOpacity( double aOpacity ) {
		setDragOpacity( aOpacity );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Node getSprite() {
		return _node;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MoveMode getMoveMode() {
		return _moveMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMoveMode( MoveMode aMoveMode ) {
		_moveMode = aMoveMode;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridDragSpriteEventHandler withMoveMode( MoveMode aMoveMode ) {
		setMoveMode( aMoveMode );
		return this;
	}

	/**
	 * Gets the move sprite duration in milliseconds.
	 *
	 * @return the drag sprite duration in milliseconds
	 */
	public int getDragSpriteDurationMillis() {
		return _dragSpriteDurationMillis;
	}

	/**
	 * Sets the drag sprite duration in milliseconds.
	 *
	 * @param aDragSpriteDurationMillis the new drag sprite duration in
	 *        milliseconds
	 */
	public void setDragSpriteDurationMillis( int aDragSpriteDurationMillis ) {
		_dragSpriteDurationMillis = aDragSpriteDurationMillis;
	}

	/**
	 * With drag sprite duration in milliseconds.
	 *
	 * @param aDragSpriteDurationMillis the drag sprite duration in milliseconds
	 * 
	 * @return this instance as of the builder pattern.
	 */
	public FxGridDragSpriteEventHandler withDragSpriteDurationMillis( int aDragSpriteDurationMillis ) {
		setDragSpriteDurationMillis( aDragSpriteDurationMillis );
		return this;
	}

	/**
	 * Gets the move sprite duration in milliseconds.
	 *
	 * @return the move sprite duration in milliseconds
	 */
	public int getMoveSpriteDurationMillis() {
		return _moveSpriteDurationMillis;
	}

	/**
	 * Sets the move sprite duration in milliseconds.
	 *
	 * @param aMoveSpriteDurationMillis the new move sprite duration in
	 *        milliseconds
	 */
	public void setMoveSpriteDurationMillis( int aMoveSpriteDurationMillis ) {
		_moveSpriteDurationMillis = aMoveSpriteDurationMillis;
	}

	/**
	 * With move sprite duration in milliseconds.
	 *
	 * @param aMoveSpriteDurationMillis the move sprite duration in milliseconds
	 * 
	 * @return this instance as of the builder pattern.
	 */
	public FxGridDragSpriteEventHandler withMoveSpriteDurationMillis( int aMoveSpriteDurationMillis ) {
		setMoveSpriteDurationMillis( aMoveSpriteDurationMillis );
		return this;
	}

	/**
	 * Sets the listener being notified upon offset changes.
	 * 
	 * @param aOffsetChangedListener The offset changed listener being provided
	 *        with any offset changes applied (as of dragging the scene).
	 */
	public void onOffsetChanged( BiConsumer<Integer, Integer> aOffsetChangedListener ) {
		_offsetChangedListener = aOffsetChangedListener;
	}

	/**
	 * Sets the listener being notified upon mouse clicks (alongside the
	 * position in the grid of the click).
	 * 
	 * @param aMouseClickedListener The listener being provided with the grid
	 *        positions of where the mouse was clicked.
	 */
	public void onMouseClicked( BiConsumer<Integer, Integer> aMouseClickedListener ) {
		_mouseClickedListener = aMouseClickedListener;
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFECYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void dispose() {
		_node.setOnMousePressed( null );
		_node.setOnMouseDragged( null );
		_node.setOnMouseReleased( null );
		_node = null;
	}

	// /////////////////////////////////////////////////////////////////////////
	// EVENT-HANDLER:
	// /////////////////////////////////////////////////////////////////////////

	private final EventHandler<MouseEvent> _onMousePressedEventHandler = ( MouseEvent aEvent ) -> { _isDragged = false; final Node theSprite = (Node) ( aEvent.getSource() ); _prevOpacity = theSprite.getOpacity(); theSprite.setOpacity( _dragOpacity ); _sceneX = aEvent.getSceneX(); _sceneY = aEvent.getSceneY(); _posX = theSprite.getTranslateX(); _posY = theSprite.getTranslateY(); LOGGER.log( Level.FINE, "Sprite mouse press X := " + aEvent.getSceneX() ); LOGGER.log( Level.FINE, "Sprite mouse press Y := " + aEvent.getSceneY() ); aEvent.consume(); };

	private final EventHandler<MouseEvent> _onMouseDraggedEventHandler = new EventHandler<MouseEvent>() {

		@Override
		public void handle( MouseEvent aEvent ) {
			final Node theSprite = (Node) ( aEvent.getSource() );
			final double theSceneOffsetX = aEvent.getSceneX() - _sceneX;
			final double theSceneOffsetY = aEvent.getSceneY() - _sceneY;
			double theTranslateX = _posX + theSceneOffsetX;
			double theTranslateY = _posY + theSceneOffsetY;
			switch ( _moveMode ) {
			case SMOOTH -> {
				theTranslateX = toBoundsX( theTranslateX, theSprite );
				theTranslateY = toBoundsY( theTranslateY, theSprite );
			}
			case JUMPY -> {
				theTranslateX = toSnapX( theTranslateX, theSprite );
				theTranslateY = toSnapY( theTranslateY, theSprite );
			}
			}
			theSprite.setTranslateX( theTranslateX );
			theSprite.setTranslateY( theTranslateY );
			final int theOffsetX = (int) Math.round( theTranslateX / ( getFieldWidth() + getFieldGap() ) );
			final int theOffsetY = (int) Math.round( theTranslateY / ( getFieldHeight() + getFieldGap() ) );
			if ( theOffsetX != _offsetX || theOffsetY != _offsetY ) {
				_offsetX = theOffsetX;
				_offsetY = theOffsetY;
				if ( _offsetChangedListener != null ) {
					_offsetChangedListener.accept( theOffsetX, theOffsetY );
				}
				_isDragged = true;
			}
			aEvent.consume();
		}
	};

	private final EventHandler<MouseEvent> _onMouseReleasedEventHandler = new EventHandler<MouseEvent>() {
		@Override
		public void handle( MouseEvent aEvent ) {
			final Node theSprite = (Node) ( aEvent.getSource() );
			final double theSceneOffsetX = aEvent.getSceneX() - _sceneX;
			final double theSceneOffsetY = aEvent.getSceneY() - _sceneY;
			final double theTranslateX = _posX + theSceneOffsetX;
			final double theTranslateY = _posY + theSceneOffsetY;
			final double theToX = toSnapX( theTranslateX, theSprite );
			final double theToY = toSnapY( theTranslateY, theSprite );
			theSprite.setOpacity( _prevOpacity );
			switch ( _moveMode ) {
			case SMOOTH -> {
				final Runnable theRunner = new Runnable() {
					@Override
					public void run() {
						final TranslateTransition theTransition = new TranslateTransition( Duration.millis( _dragSpriteDurationMillis ), theSprite );
						theTransition.setToX( theToX );
						theTransition.setToY( theToY );
						theTransition.setCycleCount( 1 );
						theTransition.setAutoReverse( false );
						theTransition.play();
					}
				};
				if ( Platform.isFxApplicationThread() ) {
					theRunner.run();
				}
				else {
					Platform.runLater( theRunner );
				}
			}
			case JUMPY -> {
				theSprite.setTranslateX( theToX );
				theSprite.setTranslateY( theToY );
			}
			}
			LOGGER.log( Level.FINE, "Sprite mouse release X := " + aEvent.getSceneX() );
			LOGGER.log( Level.FINE, "Sprite mouse release Y := " + aEvent.getSceneY() );
			LOGGER.log( Level.FINE, "Sprite mouse release offset X := " + theSceneOffsetX );
			LOGGER.log( Level.FINE, "Sprite mouse release offset Y := " + theSceneOffsetY );
			LOGGER.log( Level.FINE, "Sprite translate X := " + theTranslateX );
			LOGGER.log( Level.FINE, "Sprite translate Y := " + theTranslateY );
			aEvent.consume();

			if ( !_isDragged && _mouseClickedListener != null ) {
				final int theClickPosX = (int) ( aEvent.getSceneX() / ( getFieldWidth() + getFieldGap() ) ) - _offsetX;
				final int theClickPosY = (int) ( aEvent.getSceneY() / ( getFieldHeight() + getFieldGap() ) ) - _offsetY;
				_mouseClickedListener.accept( theClickPosX, theClickPosY );
			}
		}
	};

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private double toBoundsX( double aTranslateX, Node aSprite ) {
		// return toBounds( aTranslateX, aSprite.getBoundsInParent().getWidth(), _scene.getWidth(), _leftPadding, _rightPadding );
		return toBounds( aTranslateX, aSprite.getBoundsInParent().getWidth(), _pane.getWidth() );
	}

	private double toBoundsY( double aTranslateY, Node aSprite ) {
		// return toBounds( aTranslateY, aSprite.getBoundsInParent().getHeight(), _scene.getHeight(), _topPadding, _bottomPadding );
		return toBounds( aTranslateY, aSprite.getBoundsInParent().getHeight(), _pane.getHeight() );
	}

	//	private double toBounds( double aTranslateSize, double aSpriteSize, double aBackgroundSize, int aBeforePadding, int aAfterPadding ) {
	//		// The sprite (=sprite) is smaller than the scene:
	//		if ( aSpriteSize < aBackgroundSize ) {
	//			if ( aSpriteSize + aTranslateSize > aBackgroundSize ) aTranslateSize = aBackgroundSize - aSpriteSize;
	//			if ( aTranslateSize < 0 ) aTranslateSize = 0;
	//		}
	//		// The sprite (=background) is larger than the scene:
	//		else {
	//			if ( aSpriteSize + aTranslateSize < aBackgroundSize - aAfterPadding ) aTranslateSize = aBackgroundSize - aSpriteSize - aAfterPadding;
	//			if ( aTranslateSize > aBeforePadding ) aTranslateSize = aBeforePadding;
	//		}
	//		return aTranslateSize;
	//	}

	private double toBounds( double aTranslateSize, double aSpriteSize, double aBackgroundSize ) {
		// The sprite (=sprite) is smaller than the scene:
		if ( aSpriteSize < aBackgroundSize ) {
			if ( aSpriteSize + aTranslateSize > aBackgroundSize ) {
				aTranslateSize = aBackgroundSize - aSpriteSize;
			}
			if ( aTranslateSize < 0 ) {
				aTranslateSize = 0;
			}
		}
		// The sprite (=background) is larger than the scene:
		else {
			if ( aSpriteSize + aTranslateSize < aBackgroundSize ) {
				aTranslateSize = aBackgroundSize - aSpriteSize;
			}
			if ( aTranslateSize > 0 ) {
				aTranslateSize = 0;
			}
		}
		return aTranslateSize;
	}

	private double toSnapX( double aTranslateX, Node aSprite ) {
		// return toSnap( aTranslateX, aSprite.getBoundsInParent().getWidth(), getFieldWidth(), _scene.getWidth(), _leftPadding, _rightPadding );
		return toSnap( aTranslateX, aSprite.getBoundsInParent().getWidth(), getFieldWidth(), _pane.getWidth() );
	}

	private double toSnapY( double aTranslateY, Node aSprite ) {
		// return toSnap( aTranslateY, aSprite.getBoundsInParent().getHeight(), getFieldHeight(), _scene.getHeight(), _topPadding, _bottomPadding );
		return toSnap( aTranslateY, aSprite.getBoundsInParent().getHeight(), getFieldHeight(), _pane.getHeight() );
	}

	//	private double toSnap( double aTranslateSize, double aSpriteSize, int aFieldSize, double aBackgroundSize, int aBeforePadding, int aAfterPadding ) {
	//		if ( aSpriteSize > aBackgroundSize && aSpriteSize + aTranslateSize == aBackgroundSize ) {
	//			return aTranslateSize;
	//		}
	//		return toBounds( Math.round( aTranslateSize / (aFieldSize + getFieldGap()) ) * (aFieldSize + getFieldGap()), aSpriteSize, aBackgroundSize, aBeforePadding, aAfterPadding );
	//	}

	private double toSnap( double aTranslateSize, double aSpriteSize, int aFieldSize, double aBackgroundSize ) {
		if ( aSpriteSize > aBackgroundSize && aSpriteSize + aTranslateSize == aBackgroundSize ) {
			return aTranslateSize;
		}
		return toBounds( Math.round( aTranslateSize / ( aFieldSize + getFieldGap() ) ) * ( aFieldSize + getFieldGap() ), aSpriteSize, aBackgroundSize );
	}
}