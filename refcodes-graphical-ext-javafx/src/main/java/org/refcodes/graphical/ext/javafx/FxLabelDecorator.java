// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical.ext.javafx;

import org.refcodes.graphical.LayoutMode;
import org.refcodes.mixin.TextAccessor.TextBuilder;
import org.refcodes.mixin.TextAccessor.TextProperty;
import org.refcodes.textual.FontNameAccessor.FontNameBuilder;
import org.refcodes.textual.FontNameAccessor.FontNameProperty;
import org.refcodes.textual.HorizAlignTextMode;
import org.refcodes.textual.HorizAlignTextModeAccessor.HorizAlignTextModeBuilder;
import org.refcodes.textual.HorizAlignTextModeAccessor.HorizAlignTextModeProperty;
import org.refcodes.textual.VertAlignTextMode;
import org.refcodes.textual.VertAlignTextModeAccessor.VertAlignTextModeBuilder;
import org.refcodes.textual.VertAlignTextModeAccessor.VertAlignTextModeProperty;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Implementation of the {@link FxLabelDecorator} interface. Possible extension
 * (idea): Provide means to set cell width and height manually and make them
 * images then scale accordingly!
 */
public class FxLabelDecorator extends StackPane implements TextProperty, TextBuilder<FxLabelDecorator>, HorizAlignTextModeProperty, HorizAlignTextModeBuilder<FxLabelDecorator>, VertAlignTextModeProperty, VertAlignTextModeBuilder<FxLabelDecorator>, FontNameProperty, FontNameBuilder<FxLabelDecorator> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Daemon to be invoked either directly upon construction time via
	 * {@link RedrawDaemon#run()} or during JavaFX runtime via
	 * {@link Platform#runLater(Runnable)}.
	 */
	private class RedrawDaemon implements Runnable {

		@Override
		public void run() {
			double theWidth = getWidth();
			double theHeight = getHeight();
			if ( theWidth == 0 || theHeight == 0 ) {
				if ( _content instanceof ImageView ) {
					theWidth = ( (ImageView) _content ).getImage().getWidth();
					theHeight = ( (ImageView) _content ).getImage().getHeight();
				}
				else if ( _content instanceof FxLabelDecorator ) {
					theWidth = ( (FxLabelDecorator) _content ).getImage().getWidth();
					theHeight = ( (FxLabelDecorator) _content ).getImage().getHeight();
				}
				else if ( _content instanceof Region ) {
					theWidth = ( (Region) _content ).getPrefWidth();
					theHeight = ( (Region) _content ).getPrefHeight();
				}
			}

			setHeight( theWidth );
			setWidth( theHeight );
			setPrefWidth( theWidth );
			setPrefHeight( theHeight );
			setMinWidth( theWidth );
			setMinHeight( theHeight );

			final Font theFont = FxGraphicalUtility.toFont( _fontName, _text, _textLayoutMode == LayoutMode.VERTICAL ? theHeight * _textSizeFactor : theWidth * _textSizeFactor, _textLayoutMode );
			_textNode.setText( _text );
			_textNode.setFont( theFont );

			double theLeftTextMargin = 0;
			double theTopTextMargin = 0;
			double theRightTextMargin = 0;
			double theBottomTextMargin = 0;

			if ( _horizAlignTextMode != HorizAlignTextMode.CENTER && _horizAlignTextMode != HorizAlignTextMode.BLOCK ) {
				theLeftTextMargin = ( _textLayoutMode == LayoutMode.VERTICAL ? theHeight : theWidth ) * _leftTextMarginFactor;
				theRightTextMargin = ( _textLayoutMode == LayoutMode.VERTICAL ? theHeight : theWidth ) * _rightTextMarginFactor;
			}

			if ( _vertAlignTextMode != VertAlignTextMode.MIDDLE ) {
				theTopTextMargin = ( _textLayoutMode == LayoutMode.VERTICAL ? theHeight : theWidth ) * _topTextMarginFactor;
				theBottomTextMargin = ( _textLayoutMode == LayoutMode.VERTICAL ? theHeight : theWidth ) * _bottomTextMarginFactor;
			}

			final double theLeftTextPadding = ( _textLayoutMode == LayoutMode.VERTICAL ? theHeight : theWidth ) * _leftTextPaddingFactor;
			final double theTopTextPadding = ( _textLayoutMode == LayoutMode.VERTICAL ? theHeight : theWidth ) * _topTextPaddingFactor;
			final double theRightTextPadding = ( _textLayoutMode == LayoutMode.VERTICAL ? theHeight : theWidth ) * _rightTextPaddingFactor;
			final double theBottomTextPadding = ( _textLayoutMode == LayoutMode.VERTICAL ? theHeight : theWidth ) * _bottomTextPaddingFactor;

			final double theTextBorderArc = ( _textLayoutMode == LayoutMode.VERTICAL ? theHeight : theWidth ) * _textBorderArcFactor;
			final double theTextBorderSize = ( _textLayoutMode == LayoutMode.VERTICAL ? theHeight : theWidth ) * _textBorderSizeFactor;

			if ( _horizAlignTextMode == HorizAlignTextMode.LEFT ) {
				_textNode.setTranslateX( theLeftTextMargin + theLeftTextPadding );
			}
			if ( _horizAlignTextMode == HorizAlignTextMode.RIGHT ) {
				_textNode.setTranslateX( -( theRightTextMargin + theRightTextPadding ) );
			}
			if ( _vertAlignTextMode == VertAlignTextMode.TOP ) {
				_textNode.setTranslateY( theTopTextMargin + theTopTextPadding );
			}
			if ( _vertAlignTextMode == VertAlignTextMode.BOTTOM ) {
				_textNode.setTranslateY( -( theBottomTextMargin + theBottomTextPadding ) );
			}

			_rect.setArcWidth( theTextBorderArc );
			_rect.setArcHeight( theTextBorderArc );
			_rect.setStrokeWidth( theTextBorderSize );
			_rect.setWidth( _textNode.getBoundsInLocal().getWidth() + theLeftTextPadding + theRightTextPadding );
			_rect.setHeight( _textNode.getBoundsInLocal().getHeight() + theTopTextPadding + theBottomTextPadding );

			if ( _horizAlignTextMode == HorizAlignTextMode.LEFT ) {
				_rect.setTranslateX( theLeftTextMargin );
			}
			if ( _horizAlignTextMode == HorizAlignTextMode.RIGHT ) {
				_rect.setTranslateX( -( theRightTextMargin ) );
			}
			if ( _vertAlignTextMode == VertAlignTextMode.TOP ) {
				_rect.setTranslateY( theTopTextMargin );
			}
			if ( _vertAlignTextMode == VertAlignTextMode.BOTTOM ) {
				_rect.setTranslateY( -( theBottomTextMargin ) );
			}
		}
	}

	private static final Color DEFAULT_TEXT_BACKGROUND_COLOR = Color.BLACK;
	private static final double DEFAULT_TEXT_BORDER_ARC_FACTOR = 0.025;
	private static final Color DEFAULT_TEXT_BORDER_COLOR = Color.GRAY;
	private static final int DEFAULT_TEXT_BORDER_SIZE = 0;
	private static final Color DEFAULT_TEXT_COLOR = Color.WHITE;
	private static final int DEFAULT_TEXT_MARGIN_FACTOR = 0;
	private static final int DEFAULT_TEXT_PADDING_FACTOR = 0;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private static final int PROBE_FONT_SIZE = 24;

	/**
	 * Creates an {@link ImageView} from the {@link Image}.
	 * 
	 * @param aImage The {@link Image} from which to create the
	 *        {@link ImageView}.
	 * 
	 * @return The according{@link ImageView}
	 */
	private static ImageView toImageView( Image aImage ) {
		final ImageView theImageView = new ImageView( aImage );
		theImageView.smoothProperty().set( true );
		return theImageView;
	}

	/**
	 * Converts an object to a {@link String}. Returns the passed object as is
	 * if it is already an instance of {@link String}.
	 * 
	 * @param aText The text for which to get the {@link String} representation.
	 * 
	 * @return The object's {@link String} representation.
	 */
	private static String toString( Object aText ) {
		if ( aText == null ) {
			return null;
		}
		return aText instanceof String ? (String) aText : aText.toString();
	}

	private double _bottomTextMarginFactor = DEFAULT_TEXT_MARGIN_FACTOR;
	private double _bottomTextPaddingFactor = DEFAULT_TEXT_PADDING_FACTOR;
	private Node _content;
	private String _fontName;
	private HorizAlignTextMode _horizAlignTextMode = HorizAlignTextMode.CENTER;
	private double _leftTextMarginFactor = DEFAULT_TEXT_MARGIN_FACTOR;
	private double _leftTextPaddingFactor = DEFAULT_TEXT_PADDING_FACTOR;
	private Rectangle _rect;
	private RedrawDaemon _redraw = new RedrawDaemon();
	private double _rightTextMarginFactor = DEFAULT_TEXT_MARGIN_FACTOR;
	private double _rightTextPaddingFactor = DEFAULT_TEXT_PADDING_FACTOR;
	private String _text;
	private double _textBorderArcFactor = DEFAULT_TEXT_BORDER_ARC_FACTOR;
	private double _textBorderSizeFactor = DEFAULT_TEXT_BORDER_SIZE;
	private LayoutMode _textLayoutMode = LayoutMode.VERTICAL;
	private Text _textNode;
	private double _textSizeFactor;
	private double _topTextMarginFactor = DEFAULT_TEXT_MARGIN_FACTOR;
	private double _topTextPaddingFactor = DEFAULT_TEXT_PADDING_FACTOR;
	private VertAlignTextMode _vertAlignTextMode = VertAlignTextMode.MIDDLE;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link FxLabelDecorator} with the required minimum
	 * attributes.
	 * 
	 * @param aText The with which to initialize the cell.
	 * @param aFontName The font name to be used when rendering.
	 * @param aTextSizeFactor The factor to be used relative to the width of the
	 *        cell.
	 * @param aImage The background image to be used.
	 */
	public FxLabelDecorator( Object aText, String aFontName, double aTextSizeFactor, Image aImage ) {
		this( toString( aText ), aFontName, aTextSizeFactor, aImage );
	}

	/**
	 * Constructs a {@link FxLabelDecorator} with the required minimum
	 * attributes.
	 * 
	 * @param aText The with which to initialize the cell.
	 * @param aFontName The font name to be used when rendering.
	 * @param aTextSizeFactor The factor to be used relative to the width of the
	 *        cell.
	 * @param aImage The background image to be used.
	 */
	public FxLabelDecorator( String aText, String aFontName, double aTextSizeFactor, Image aImage ) {
		this( aText, aFontName, aTextSizeFactor, toImageView( aImage ) );
	}

	/**
	 * Constructs a {@link FxLabelDecorator} with the required minimum
	 * attributes.
	 * 
	 * @param aText The with which to initialize the cell.
	 * @param aFontName The font name to be used when rendering.
	 * @param aTextSizeFactor The factor to be used relative to the width of the
	 *        cell.
	 * @param aNode The background image to be used.
	 */
	public FxLabelDecorator( String aText, String aFontName, double aTextSizeFactor, Node aNode ) {
		_text = aText;
		_fontName = aFontName;
		_textSizeFactor = aTextSizeFactor;
		_content = aNode;
		_textNode = new Text( aText );
		_textNode.setFont( new Font( aFontName, PROBE_FONT_SIZE ) );
		_textNode.setFill( DEFAULT_TEXT_COLOR );
		_rect = new Rectangle( _textNode.getBoundsInLocal().getWidth(), _textNode.getBoundsInLocal().getHeight(), DEFAULT_TEXT_BACKGROUND_COLOR );
		_rect.setStroke( DEFAULT_TEXT_BORDER_COLOR );
		setBorder( null );
		StackPane.setMargin( _content, null );
		StackPane.setAlignment( _content, Pos.CENTER );
		StackPane.setMargin( _rect, null );
		StackPane.setAlignment( _rect, FxGraphicalUtility.toPos( _horizAlignTextMode, _vertAlignTextMode ) );
		StackPane.setMargin( _textNode, null );
		StackPane.setAlignment( _textNode, FxGraphicalUtility.toPos( _horizAlignTextMode, _vertAlignTextMode ) );
		getChildren().addAll( _rect, _content, _textNode );
		_redraw.run();
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the bottom margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @return The bottom margin factor relative to the size of the cell of the
	 *         text from the text box.
	 */
	public double getBottomTextMarginFactor() {
		return _bottomTextMarginFactor;
	}

	/**
	 * Returns the bottom padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @return The bottom padding factor relative to the size of the cell of the
	 *         text from the text box.
	 */
	public double getBottomTextPaddingFactor() {
		return _bottomTextPaddingFactor;
	}

	/**
	 * Returns the node of the cell.
	 * 
	 * @return The node of the cell.
	 */
	public Node getContent() {
		return _content;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getFontName() {
		return _textNode.getFont().getName();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorizAlignTextMode getHorizAlignTextMode() {
		return _horizAlignTextMode;
	}

	/**
	 * Returns the image of the cell.
	 * 
	 * @return The image of the cell.
	 */
	public Image getImage() {
		if ( _content instanceof ImageView ) {
			return ( (ImageView) _content ).getImage();
		}
		else if ( _content instanceof FxLabelDecorator ) {
			return ( (FxLabelDecorator) _content ).getImage();
		}
		return null;
	}

	/**
	 * Returns the left margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @return The left margin factor relative to the size of the cell of the
	 *         text from the text box.
	 */
	public double getLeftTextMarginFactor() {
		return _leftTextMarginFactor;
	}

	/**
	 * Returns the left padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @return The left padding factor relative to the size of the cell of the
	 *         text from the text box.
	 */
	public double getLeftTextPaddingFactor() {
		return _leftTextPaddingFactor;
	}

	/**
	 * Returns the right margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @return The right margin factor relative to the size of the cell of the
	 *         text from the text box.
	 */
	public double getRightTextMarginFactor() {
		return _rightTextMarginFactor;
	}

	/**
	 * Returns the right padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @return The right padding factor relative to the size of the cell of the
	 *         text from the text box.
	 */
	public double getRightTextPaddingFactor() {
		return _rightTextPaddingFactor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getText() {
		return _text;
	}

	/**
	 * Returns the background of the text.
	 * 
	 * @return The background of the text inside the cell.
	 */
	public Paint getTextBackground() {
		return _rect.getFill();
	}

	/**
	 * Returns the arc of the text border relative to the arc of the cell (as of
	 * {@link #getTextLayoutMode()}). Values form 0 to 1 represents width
	 * between 0% and 100%.
	 * 
	 * @return The factor being used relative to the arc of the cell.
	 */
	public double getTextBorderArcFactor() {
		return _textBorderArcFactor;
	}

	/**
	 * Returns the color of the text border.
	 * 
	 * @return The color of the text border inside the cell.
	 */
	public Paint getTextBorderColor() {
		return _rect.getStroke();
	}

	/**
	 * Returns the size of the text border relative to the size of the cell (as
	 * of {@link #getTextLayoutMode()}). Values form 0 to 1 represents width
	 * between 0% and 100%.
	 * 
	 * @return The factor being used relative to the size of the cell.
	 */
	public double getTextBorderSizeFactor() {
		return _textBorderSizeFactor;
	}

	/**
	 * Returns the color of the text.
	 * 
	 * @return The color of the text inside the cell.
	 */
	public Paint getTextColor() {
		return _textNode.getFill();
	}

	/**
	 * Returns the {@link LayoutMode} for the text metrics.
	 * 
	 * @return The {@link LayoutMode} to be applied to text metrics. settings.
	 */
	public LayoutMode getTextLayoutMode() {
		return _textLayoutMode;
	}

	/**
	 * Returns the size of the text relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}). Values form 0 to 1 represents width
	 * between 0% and 100%.
	 * 
	 * @return The factor being used relative to the size of the cell.
	 */
	public double getTextSizeFactor() {
		return _textSizeFactor;
	}

	/**
	 * Returns the top margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @return The top margin factor relative to the size of the cell of the
	 *         text from the text box.
	 */
	public double getTopTextMarginFactor() {
		return _topTextMarginFactor;
	}

	/**
	 * Returns the top padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @return The top padding factor relative to the size of the cell of the
	 *         text from the text box.
	 */
	public double getTopTextPaddingFactor() {
		return _topTextPaddingFactor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public VertAlignTextMode getVertAlignTextMode() {
		return _vertAlignTextMode;
	}

	/**
	 * Sets the bottom margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aBottomTextMarginFactor The bottom margin factor of the text from
	 *        the text box.
	 */
	public void setBottomTextMarginFactor( double aBottomTextMarginFactor ) {
		_bottomTextMarginFactor = aBottomTextMarginFactor;
	}

	/**
	 * Sets the bottom padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aBottomTextPaddingFactor The bottom padding factor of the text
	 *        from the text box.
	 */
	public void setBottomTextPaddingFactor( double aBottomTextPaddingFactor ) {
		_bottomTextPaddingFactor = aBottomTextPaddingFactor;

	}

	/**
	 * Sets the node of the cell.
	 * 
	 * @param aContent The node of the cell.
	 */
	public void setContent( Node aContent ) {
		if ( aContent instanceof Region ) {
			( (Region) aContent ).setPrefWidth( getWidth() );
			( (Region) aContent ).setPrefHeight( getHeight() );
		}
		_content = aContent;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFontName( String aFontName ) {
		_fontName = aFontName;
		redraw();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		_horizAlignTextMode = aHorizAlignTextMode;
		final Pos thePos = FxGraphicalUtility.toPos( _horizAlignTextMode, _vertAlignTextMode );
		StackPane.setAlignment( _textNode, thePos );
		StackPane.setAlignment( _rect, thePos );
	}

	/**
	 * Sets the horizontal margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTextMarginFactor The horizontal margin factor of the text from
	 *        the text box.
	 */
	public void setHorizTextMarginFactor( double aTextMarginFactor ) {
		setLeftTextMarginFactor( aTextMarginFactor );
		setRightTextMarginFactor( aTextMarginFactor );
	}

	/**
	 * Sets the horizontal padding factor relative to the size of the cell (as
	 * of {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTextPaddingFactor The horizontal padding factor of the text from
	 *        the text box.
	 */
	public void setHorizTextPaddingFactor( double aTextPaddingFactor ) {
		setLeftTextPaddingFactor( aTextPaddingFactor );
		setRightTextPaddingFactor( aTextPaddingFactor );
	}

	/**
	 * Sets the image of the cell.
	 * 
	 * @param aImage The image of the cell.
	 */
	public void setImage( Image aImage ) {
		if ( _content instanceof ImageView ) {
			( (ImageView) _content ).setImage( aImage );
		}
		else if ( _content instanceof FxLabelDecorator ) {
			( (FxLabelDecorator) _content ).setImage( aImage );
		}
		else {
			_content = toImageView( aImage );
			redraw();
		}
	}

	/**
	 * Sets the left margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aLeftTextMarginFactor The left margin factor of the text from the
	 *        text box.
	 */
	public void setLeftTextMarginFactor( double aLeftTextMarginFactor ) {
		_leftTextMarginFactor = aLeftTextMarginFactor;

	}

	/**
	 * Sets the left padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aLeftTextPaddingFactor The left padding factor of the text from
	 *        the text box.
	 */
	public void setLeftTextPaddingFactor( double aLeftTextPaddingFactor ) {
		_leftTextPaddingFactor = aLeftTextPaddingFactor;
	}

	/**
	 * Sets the right margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aRightTextMarginFactor The right margin factor of the text from
	 *        the text box.
	 */
	public void setRightTextMarginFactor( double aRightTextMarginFactor ) {
		_rightTextMarginFactor = aRightTextMarginFactor;
	}

	/**
	 * Sets the right padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aRightTextPaddingFactor The right padding factor of the text from
	 *        the text box.
	 */
	public void setRightTextPaddingFactor( double aRightTextPaddingFactor ) {
		_rightTextPaddingFactor = aRightTextPaddingFactor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setText( String aText ) {
		_text = aText;
		redraw();
	}

	/**
	 * Sets the background of the text.
	 * 
	 * @param aTextBackground The background of the text inside the cell.
	 */
	public void setTextBackground( Paint aTextBackground ) {
		_rect.setFill( aTextBackground );

	}

	/**
	 * Sets the arc of the text border relative to the arc of the cell (as of
	 * {@link #getTextLayoutMode()}). Values form 0 to 1 represents width
	 * between 0% and 100%.
	 * 
	 * @param aTextBorderArcFactor The factor to be used relative to the arc of
	 *        the cell.
	 */
	public void setTextBorderArcFactor( double aTextBorderArcFactor ) {
		_textBorderArcFactor = aTextBorderArcFactor;
		redraw();
	}

	/**
	 * Sets the color of the text border.
	 * 
	 * @param aTextBorderColor The color of the text border inside the cell.
	 */
	public void setTextBorderColor( Paint aTextBorderColor ) {
		_rect.setStroke( aTextBorderColor );
	}

	/**
	 * Sets the size of the text border relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}). Values form 0 to 1 represents width
	 * between 0% and 100%.
	 * 
	 * @param aTextBorderSizeFactor The factor to be used relative to the size
	 *        of the cell.
	 */
	public void setTextBorderSizeFactor( double aTextBorderSizeFactor ) {
		_textBorderSizeFactor = aTextBorderSizeFactor;
		redraw();
	}

	/**
	 * Sets the color of the text.
	 *
	 * @param aColor the new text color
	 */
	public void setTextColor( Paint aColor ) {
		_textNode.setFill( aColor );

	}

	/**
	 * Sets the {@link LayoutMode} for the text metrics.
	 * 
	 * @param aLayoutMode The {@link LayoutMode} to be applied to text metrics.
	 *        settings.
	 */
	public void setTextLayoutMode( LayoutMode aLayoutMode ) {
		_textLayoutMode = aLayoutMode;
	}

	/**
	 * Sets the margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTextMarginFactor The margin factor of the text from the text box.
	 */
	public void setTextMarginFactor( double aTextMarginFactor ) {
		setTopTextMarginFactor( aTextMarginFactor );
		setLeftTextMarginFactor( aTextMarginFactor );
		setBottomTextMarginFactor( aTextMarginFactor );
		setRightTextMarginFactor( aTextMarginFactor );
	}

	/**
	 * Sets the padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTextPaddingFactor The padding factor of the text from the text
	 *        box.
	 */
	public void setTextPaddingFactor( double aTextPaddingFactor ) {
		setTopTextPaddingFactor( aTextPaddingFactor );
		setLeftTextPaddingFactor( aTextPaddingFactor );
		setBottomTextPaddingFactor( aTextPaddingFactor );
		setRightTextPaddingFactor( aTextPaddingFactor );
	}

	/**
	 * Sets the size of the text relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}). Values form 0 to 1 represents width
	 * between 0% and 100%.
	 * 
	 * @param aTextSizeFactor The factor to be used relative to the size of the
	 *        cell.
	 */
	public void setTextSizeFactor( double aTextSizeFactor ) {
		_textSizeFactor = aTextSizeFactor;
		redraw();
	}

	/**
	 * Sets the top margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTopTextMarginFactor The top margin factor of the text from the
	 *        text box.
	 */
	public void setTopTextMarginFactor( double aTopTextMarginFactor ) {
		_topTextMarginFactor = aTopTextMarginFactor;

	}

	/**
	 * Sets the top padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTopTextPaddingFactor The top padding factor of the text from the
	 *        text box.
	 */
	public void setTopTextPaddingFactor( double aTopTextPaddingFactor ) {
		_topTextPaddingFactor = aTopTextPaddingFactor;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setVertAlignTextMode( VertAlignTextMode aVertAlignTextMode ) {
		_vertAlignTextMode = aVertAlignTextMode;
		final Pos thePos = FxGraphicalUtility.toPos( _horizAlignTextMode, _vertAlignTextMode );
		StackPane.setAlignment( _textNode, thePos );
		StackPane.setAlignment( _rect, thePos );
	}

	/**
	 * Sets the vertical margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTextMarginFactor The vertical margin factor of the text from the
	 *        text box.
	 */
	public void setVertTextMarginFactor( double aTextMarginFactor ) {
		setTopTextMarginFactor( aTextMarginFactor );
		setBottomTextMarginFactor( aTextMarginFactor );
	}

	/**
	 * Sets the vertical padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTextPaddingFactor The vertical padding factor of the text from
	 *        the text box.
	 */
	public void setVertTextPaddingFactor( double aTextPaddingFactor ) {
		setTopTextPaddingFactor( aTextPaddingFactor );
		setBottomTextPaddingFactor( aTextPaddingFactor );
	}

	/**
	 * Sets the bottom margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTextMarginFactor The bottom margin factor relative to the size of
	 *        the cell of the text box from the cell
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withBottomTextMarginFactor( double aTextMarginFactor ) {
		setTextMarginFactor( aTextMarginFactor );
		return this;
	}

	/**
	 * Sets the bottom padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTextPaddingFactor The bottom padding factor relative to the size
	 *        of the cell of the text from the text box
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withBottomTextPaddingFactor( double aTextPaddingFactor ) {
		setTextPaddingFactor( aTextPaddingFactor );
		return this;
	}

	/**
	 * Sets the node of the cell.
	 * 
	 * @param aContent The color to be used.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withContent( Node aContent ) {
		setContent( aContent );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxLabelDecorator withFontName( String aFontName ) {
		setFontName( aFontName );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxLabelDecorator withHorizAlignTextMode( HorizAlignTextMode aHorizAlignTextMode ) {
		setHorizAlignTextMode( aHorizAlignTextMode );
		return this;
	}

	/**
	 * Sets the horizontal margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTextMarginFactor The horizontal margin factor relative to the
	 *        size of the cell of the text box from the cell
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withHorizTextMarginFactor( double aTextMarginFactor ) {
		setHorizTextMarginFactor( aTextMarginFactor );
		return this;
	}

	/**
	 * Sets the horizontal padding factor relative to the size of the cell (as
	 * of {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTextPaddingFactor The horizontal padding factor relative to the
	 *        size of the cell of the text from the text box
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withHorizTextPaddingFactor( double aTextPaddingFactor ) {
		setHorizTextPaddingFactor( aTextPaddingFactor );
		return this;
	}

	/**
	 * Sets the image of the cell.
	 * 
	 * @param aImage The color to be used.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withImage( Image aImage ) {
		setImage( aImage );
		return this;
	}

	/**
	 * Sets the left margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTextMarginFactor The left margin factor relative to the size of
	 *        the cell of the text box from the cell
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withLeftTextMarginFactor( double aTextMarginFactor ) {
		setTextMarginFactor( aTextMarginFactor );
		return this;
	}

	/**
	 * Sets the left padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTextPaddingFactor The left padding factor relative to the size of
	 *        the cell of the text from the text box
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withLeftTextPaddingFactor( double aTextPaddingFactor ) {
		setTextPaddingFactor( aTextPaddingFactor );
		return this;
	}

	/**
	 * Sets the right margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTextMarginFactor The right margin factor relative to the size of
	 *        the cell of the text box from the cell
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withRightTextMarginFactor( double aTextMarginFactor ) {
		setTextMarginFactor( aTextMarginFactor );
		return this;
	}

	/**
	 * Sets the right padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTextPaddingFactor The right padding factor relative to the size
	 *        of the cell of the text from the text box
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withRightTextPaddingFactor( double aTextPaddingFactor ) {
		setTextPaddingFactor( aTextPaddingFactor );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxLabelDecorator withText( String aText ) {
		setText( aText );
		return this;
	}

	/**
	 * Sets the background of the text.
	 * 
	 * @param aTextBackground The background to be used.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTextBackground( Paint aTextBackground ) {
		setTextBackground( aTextBackground );
		return this;
	}

	/**
	 * Sets the arc of the text border relative to the arc of the cell (as of
	 * {@link #getTextLayoutMode()}). Values form 0 to 1 represents width
	 * between 0% and 100%.
	 * 
	 * @param aTextBorderArcFactor The factor to be used relative to the arc of
	 *        the cell.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTextBorderArcFactor( double aTextBorderArcFactor ) {
		setTextBorderArcFactor( aTextBorderArcFactor );
		return this;
	}

	/**
	 * Sets the color of the text border.
	 * 
	 * @param aTextBorderColor The color to be used.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTextBorderColor( Paint aTextBorderColor ) {
		setTextBorderColor( aTextBorderColor );
		return this;
	}

	/**
	 * Sets the size of the text border relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}). Values form 0 to 1 represents width
	 * between 0% and 100%.
	 * 
	 * @param aTextBorderSizeFactor The factor to be used relative to the size
	 *        of the cell.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTextBorderSizeFactor( double aTextBorderSizeFactor ) {
		setTextBorderSizeFactor( aTextBorderSizeFactor );
		return this;
	}

	/**
	 * Sets the color of the text.
	 * 
	 * @param aTextColor The color to be used.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTextColor( Paint aTextColor ) {
		setTextColor( aTextColor );
		return this;
	}

	/**
	 * Sets the {@link LayoutMode} for the text metrics.
	 * 
	 * @param aLayoutMode The {@link LayoutMode} to be applied to text metrics.
	 *        settings.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTextLayoutMode( LayoutMode aLayoutMode ) {
		setTextLayoutMode( aLayoutMode );
		return this;
	}

	/**
	 * Sets the margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTextMarginFactor The margin factor relative to the size of the
	 *        cell of the text box from the cell
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTextMarginFactor( double aTextMarginFactor ) {
		setTextMarginFactor( aTextMarginFactor );
		return this;
	}

	/**
	 * Sets the padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTextPaddingFactor The padding factor relative to the size of the
	 *        cell of the text from the text box
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTextPaddingFactor( double aTextPaddingFactor ) {
		setTextPaddingFactor( aTextPaddingFactor );
		return this;
	}

	/**
	 * Sets the size of the text relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}). Values form 0 to 1 represents width
	 * between 0% and 100%.
	 * 
	 * @param aTextSizeFactor The factor to be used relative to the size of the
	 *        cell.
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTextSizeFactor( double aTextSizeFactor ) {
		setTextSizeFactor( aTextSizeFactor );
		return this;
	}

	/**
	 * Sets the top margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTextMarginFactor The top margin factor relative to the size of
	 *        the cell of the text box from the cell
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTopTextMarginFactor( double aTextMarginFactor ) {
		setTextMarginFactor( aTextMarginFactor );
		return this;
	}

	/**
	 * Sets the top padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTextPaddingFactor The top padding factor relative to the size of
	 *        the cell of the text from the text box
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withTopTextPaddingFactor( double aTextPaddingFactor ) {
		setTextPaddingFactor( aTextPaddingFactor );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxLabelDecorator withVertAlignTextMode( VertAlignTextMode aVertAlignTextMode ) {
		setVertAlignTextMode( aVertAlignTextMode );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Sets the vertical margin factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text box from the cell.
	 * 
	 * @param aTextMarginFactor The vertical margin factor relative to the size
	 *        of the cell of the text box from the cell
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withVertTextMarginFactor( double aTextMarginFactor ) {
		setVertTextMarginFactor( aTextMarginFactor );
		return this;
	}

	/**
	 * Sets the vertical padding factor relative to the size of the cell (as of
	 * {@link #getTextLayoutMode()}) of the text from the text box.
	 * 
	 * @param aTextPaddingFactor The vertical padding factor relative to the
	 *        size of the cell of the text from the text box
	 * 
	 * @return The builder for applying multiple build operations.
	 */
	public FxLabelDecorator withVertTextPaddingFactor( double aTextPaddingFactor ) {
		setVertTextPaddingFactor( aTextPaddingFactor );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Recalculates the metrics of the elements of this node.
	 */
	protected void redraw() {
		Platform.runLater( _redraw );

	}
}
