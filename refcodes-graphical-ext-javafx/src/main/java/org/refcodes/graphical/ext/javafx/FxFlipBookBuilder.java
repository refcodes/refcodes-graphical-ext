package org.refcodes.graphical.ext.javafx;

import java.util.ArrayList;
import java.util.List;

import org.refcodes.component.LifecycleStatus;
import org.refcodes.graphical.FlipBookBuilder;

import javafx.animation.PauseTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

/**
 * The class FxFlipBookBuilder {@link AnimationDaemon} implements animated image
 * nodes similar to an animated GIF.
 */
public class FxFlipBookBuilder extends ImageView implements FlipBookBuilder<Image> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final List<Image> _startUpImages = new ArrayList<>();
	private final List<Image> _mainLoopImages = new ArrayList<>();
	private final List<Image> _ceaseImages = new ArrayList<>();
	private LifecycleStatus _lifeCycleRequest = LifecycleStatus.NONE;
	private LifecycleStatus _lifeCycleStatus = LifecycleStatus.INITIALIZED;
	private int _imageDurationInMs = 250;
	private int _startUpDelayInMs = 1000;
	private PauseTransition _transition = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new fx flip book builder impl.
	 */
	public FxFlipBookBuilder() {
		setVisible( false );
	}

	// /////////////////////////////////////////////////////////////////////////
	// BUILDERS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addStartUpImage( Image aImage ) {
		if ( _startUpImages.size() == 0 ) {
			setImage( aImage );
		}
		_startUpImages.add( aImage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addMainLoopImage( Image aImage ) {
		if ( _mainLoopImages.size() == 0 && _startUpImages.size() == 0 ) {
			setImage( aImage );
		}
		_mainLoopImages.add( aImage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addCeaseImage( Image aImage ) {
		if ( _ceaseImages.size() == 0 && _mainLoopImages.size() == 0 && _startUpImages.size() == 0 ) {
			setImage( aImage );
		}
		_ceaseImages.add( aImage );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setImageDurationInMs( int aDurationInMs ) {
		_imageDurationInMs = aDurationInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getImageDurationInMs() {
		return _imageDurationInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setStartUpDelayInMs( int aStartUpDelayInMs ) {
		_startUpDelayInMs = aStartUpDelayInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getStartUpDelayInMs() {
		return _startUpDelayInMs;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxFlipBookBuilder withImageDurationInMs( int aDurationInMs ) {
		setImageDurationInMs( aDurationInMs );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxFlipBookBuilder withStartUpDelayInMs( int aStartUpDelayInMs ) {
		setStartUpDelayInMs( aStartUpDelayInMs );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFE-CYCLE:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void initialize() {
		_lifeCycleStatus = LifecycleStatus.INITIALIZED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void start() {
		if ( _lifeCycleStatus != LifecycleStatus.INITIALIZED || _startUpImages.size() == 0 ) {
			_lifeCycleStatus = LifecycleStatus.STARTED;
		}
		if ( _transition != null ) {
			throw new IllegalStateException( "The flip book must not be started again after it has been started but not stopped (destroyed) again" );
		}
		final Runnable theRunner = new AnimationDaemon();
		if ( Platform.isFxApplicationThread() ) {
			theRunner.run();
		}
		else {
			Platform.runLater( theRunner );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void pause() {
		_lifeCycleRequest = LifecycleStatus.PAUSED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void resume() {
		_lifeCycleRequest = LifecycleStatus.STARTED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stop() {
		_lifeCycleRequest = LifecycleStatus.STOPPED;
		final PauseTransition theTransition = _transition;
		if ( theTransition != null ) {
			_transition = null;
			theTransition.stop();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void cease() {
		_lifeCycleRequest = LifecycleStatus.CEASED;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		stop();
		_startUpImages.clear();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private class AnimationDaemon implements Runnable {

		private int _index = 0;

		@Override
		public void run() {
			_transition = new PauseTransition();
			if ( _lifeCycleStatus != LifecycleStatus.INITIALIZED || _startUpDelayInMs <= 0 ) {
				flipImage();
				_transition.setDuration( Duration.millis( _imageDurationInMs ) );
			}
			else {
				_transition.setDuration( Duration.millis( _startUpDelayInMs ) );
			}
			_transition.setCycleCount( 1 );
			_transition.play();
			final EventHandler<ActionEvent> onFinished = ( ActionEvent t ) -> {
				flipImage();
				final PauseTransition theTransition = _transition;
				if ( theTransition != null ) {
					theTransition.setDuration( Duration.millis( _imageDurationInMs ) );
					theTransition.play();
				}
			};
			_transition.setOnFinished( onFinished );
		}

		private void flipImage() {
			setVisible( true );
			List<Image> theImages = null;
			switch ( _lifeCycleStatus ) {
			case CEASED -> theImages = _ceaseImages;
			case DESTROYED -> {
			}
			case INITIALIZED -> {
				theImages = _startUpImages;
				if ( _index > theImages.size() - 1 ) {
					_index = 0;
					_lifeCycleStatus = LifecycleStatus.STARTED;
					_lifeCycleRequest = LifecycleStatus.NONE;
					theImages = _mainLoopImages;
				}
			}
			case NONE -> {
			}
			case PAUSED -> {
			}
			case STARTED -> theImages = _mainLoopImages;
			case STOPPED -> {
			}
			default -> {
			}
			}
			switch ( _lifeCycleRequest ) {
			case CEASED -> {
				if ( _index > theImages.size() - 1 ) {
					_index = 0;
					_lifeCycleStatus = LifecycleStatus.CEASED;
					_lifeCycleRequest = LifecycleStatus.NONE;
					theImages = _ceaseImages;
				}
			}
			case DESTROYED -> {
			}
			case INITIALIZED -> {
				_index = 0;
				_lifeCycleStatus = LifecycleStatus.INITIALIZED;
				_lifeCycleRequest = LifecycleStatus.NONE;
				theImages = _startUpImages;
			}
			case NONE -> {
			}
			case PAUSED -> {
				return;
			}
			case STARTED -> {
			}
			case STOPPED -> {
				if ( _index > theImages.size() - 1 ) {
					_index = 0;
					_lifeCycleStatus = LifecycleStatus.STOPPED;
					_lifeCycleRequest = LifecycleStatus.NONE;
				}
				return;
			}
			default -> {
			}
			}
			if ( theImages.size() > 0 ) {
				if ( _index > theImages.size() - 1 ) {
					_index = 0;
				}
				setImage( theImages.get( _index++ ) );
			}
		}
	}
}
