package org.refcodes.graphical.ext.javafx;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import org.refcodes.exception.UnhandledEnumBugException;
import org.refcodes.graphical.Dimension;
import org.refcodes.graphical.DimensionImpl;
import org.refcodes.graphical.PixGridPane;
import org.refcodes.graphical.PixelShape;
import org.refcodes.graphical.Rectangle;
import org.refcodes.graphical.RectangleImpl;
import org.refcodes.graphical.RgbColor;
import org.refcodes.graphical.RgbPixel;
import org.refcodes.graphical.RgbPixelImpl;
import org.refcodes.graphical.RgbPixmap.RgbPixmapBuilder;
import org.refcodes.graphical.RgbPixmapBuilderImpl;

import javafx.application.Platform;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

/**
 * The {@link FxPixGridMatrixPane} uses a {@link Pane} to project a
 * {@link PixGridPane} in a JvaFx application.
 */
public class FxPixGridMatrixPane extends Pane implements PixGridPane {

	protected RgbPixmapBuilder _rgbPixmapBuilder = null;

	//	private static final float PIXEL_WIDTH_TO_HORIZONTAL_SPACE_RATIO = 0.2F;
	//	private static final float PIXEL_HEIGHT_TO_VERTICAL_SPACE_RATIO = 0.2F;
	private int _matrixHeight = 1;
	private int _matrixWidth = 1;
	private Shape[][] _shapes;
	private int _topBorder = 0;
	private int _bottomBorder = 0;
	private int _leftBorder = 0;
	private int _rightBorder = 0;
	private int _horizontalSpace = 0;
	private int _verticalSpace = 0;
	private int _pixelWidth = 1;
	private int _pixelHeight = 1;
	protected Color _inactivePixelColor;
	private int _pixmapOffsetX;
	private int _pixmapOffsetY;
	private boolean _isHorizontalWrap;
	private boolean _isVerticalWrap;
	private PixelShape _pixelShape = PixelShape.RECTANGLE;
	private List<Rectangle> _unconsideredAreaList;
	private Color _pixelBorderColor;
	private float _pixelBorderWidth;

	private static final Map<RgbPixel, Color> _pixelToFxColorCache = new WeakHashMap<>();
	private static final Map<Color, RgbPixel> _fxColorToPixelCache = new WeakHashMap<>();

	/**
	 * Instantiates a new {@link FxPixGridMatrixPane}.
	 *
	 * @param aMatrixWidth the a matrix width
	 * @param aMatrixHeight the a matrix height
	 * @param aBackgroundColor the a background color
	 * @param aPixelWidth the a pixel width
	 * @param aPixelHeight the a pixel height
	 * @param aPixelShape the a pixel shape
	 * @param aPixelBorderWidth the a pixel border width
	 * @param aPixelBorderColor the a pixel border color
	 * @param aInactivePixelColor the a inactive pixel color
	 * @param aTopBorder the a top border
	 * @param aBottomBorder the a bottom border
	 * @param aLeftBorder the a left border
	 * @param aRightBorder the a right border
	 * @param aHorizontalSpace the a horizontal space
	 * @param aPixelWidthToHorizontalSpaceRatio the a pixel width to horizontal
	 *        space ratio
	 * @param isForceHorizontalSpace the is force horizontal space
	 * @param aVerticalSpace the a vertical space
	 * @param aPixelHeightToVerticalSpaceRatio the a pixel height to vertical
	 *        space ratio
	 * @param isForceVerticalSpace the is force vertical space
	 */
	public FxPixGridMatrixPane( int aMatrixWidth, int aMatrixHeight, java.awt.Color aBackgroundColor, int aPixelWidth, int aPixelHeight, PixelShape aPixelShape, float aPixelBorderWidth, java.awt.Color aPixelBorderColor, java.awt.Color aInactivePixelColor, int aTopBorder, int aBottomBorder, int aLeftBorder, int aRightBorder, int aHorizontalSpace, float aPixelWidthToHorizontalSpaceRatio, boolean isForceHorizontalSpace, int aVerticalSpace, float aPixelHeightToVerticalSpaceRatio, boolean isForceVerticalSpace ) {
		this( aMatrixWidth, aMatrixHeight, aBackgroundColor != null ? toFxColor( aBackgroundColor ) : null, aPixelWidth, aPixelHeight, aPixelShape, aPixelBorderWidth, aPixelBorderColor != null ? toFxColor( aPixelBorderColor ) : null, aInactivePixelColor != null ? toFxColor( aInactivePixelColor ) : null, aTopBorder, aBottomBorder, aLeftBorder, aRightBorder, aHorizontalSpace, aPixelWidthToHorizontalSpaceRatio, isForceHorizontalSpace, aVerticalSpace, aPixelHeightToVerticalSpaceRatio, isForceVerticalSpace );
	}

	/**
	 * Instantiates a new {@link FxPixGridMatrixPane}.
	 *
	 * @param aMatrixWidth the a matrix width
	 * @param aMatrixHeight the a matrix height
	 * @param aBackgroundColor the a background color
	 * @param aPixelWidth the a pixel width
	 * @param aPixelHeight the a pixel height
	 * @param aPixelShape the a pixel shape
	 * @param aPixelBorderWidth the a pixel border width
	 * @param aPixelBorderColor the a pixel border color
	 * @param aInactivePixelColor the a inactive pixel color
	 * @param aTopBorder the a top border
	 * @param aBottomBorder the a bottom border
	 * @param aLeftBorder the a left border
	 * @param aRightBorder the a right border
	 * @param aHorizontalSpace the a horizontal space
	 * @param aPixelWidthToHorizontalSpaceRatio the a pixel width to horizontal
	 *        space ratio
	 * @param isForceHorizontalSpace the is force horizontal space
	 * @param aVerticalSpace the a vertical space
	 * @param aPixelHeightToVerticalSpaceRatio the a pixel height to vertical
	 *        space ratio
	 * @param isForceVerticalSpace the is force vertical space
	 */
	public FxPixGridMatrixPane( int aMatrixWidth, int aMatrixHeight, String aBackgroundColor, int aPixelWidth, int aPixelHeight, PixelShape aPixelShape, float aPixelBorderWidth, String aPixelBorderColor, String aInactivePixelColor, int aTopBorder, int aBottomBorder, int aLeftBorder, int aRightBorder, int aHorizontalSpace, float aPixelWidthToHorizontalSpaceRatio, boolean isForceHorizontalSpace, int aVerticalSpace, float aPixelHeightToVerticalSpaceRatio, boolean isForceVerticalSpace ) {
		this( aMatrixWidth, aMatrixHeight, aBackgroundColor != null ? toFxColor( aBackgroundColor ) : null, aPixelWidth, aPixelHeight, aPixelShape, aPixelBorderWidth, aPixelBorderColor != null ? toFxColor( aPixelBorderColor ) : null, aInactivePixelColor != null ? toFxColor( aInactivePixelColor ) : null, aTopBorder, aBottomBorder, aLeftBorder, aRightBorder, aHorizontalSpace, aPixelWidthToHorizontalSpaceRatio, isForceHorizontalSpace, aVerticalSpace, aPixelHeightToVerticalSpaceRatio, isForceVerticalSpace );
	}

	/**
	 * Instantiates a new {@link FxPixGridMatrixPane}.
	 *
	 * @param aMatrixWidth the a matrix width
	 * @param aMatrixHeight the a matrix height
	 * @param aBackgroundColor the a background color
	 * @param aPixelWidth the a pixel width
	 * @param aPixelHeight the a pixel height
	 * @param aPixelShape the a pixel shape
	 * @param aPixelBorderWidth the a pixel border width
	 * @param aPixelBorderColor the a pixel border color
	 * @param aInactivePixelColor the a inactive pixel color
	 * @param aTopBorder the a top border
	 * @param aBottomBorder the a bottom border
	 * @param aLeftBorder the a left border
	 * @param aRightBorder the a right border
	 * @param aHorizontalSpace the a horizontal space
	 * @param aPixelWidthToHorizontalSpaceRatio the a pixel width to horizontal
	 *        space ratio
	 * @param isForceHorizontalSpace the is force horizontal space
	 * @param aVerticalSpace the a vertical space
	 * @param aPixelHeightToVerticalSpaceRatio the a pixel height to vertical
	 *        space ratio
	 * @param isForceVerticalSpace the is force vertical space
	 */
	public FxPixGridMatrixPane( int aMatrixWidth, int aMatrixHeight, Color aBackgroundColor, int aPixelWidth, int aPixelHeight, PixelShape aPixelShape, float aPixelBorderWidth, Color aPixelBorderColor, Color aInactivePixelColor, int aTopBorder, int aBottomBorder, int aLeftBorder, int aRightBorder, int aHorizontalSpace, float aPixelWidthToHorizontalSpaceRatio, boolean isForceHorizontalSpace, int aVerticalSpace, float aPixelHeightToVerticalSpaceRatio, boolean isForceVerticalSpace ) {
		_inactivePixelColor = aInactivePixelColor != null ? aInactivePixelColor : Color.DARKGRAY;
		_pixmapOffsetX = 0;
		_pixmapOffsetY = 0;
		_isHorizontalWrap = true;
		_isVerticalWrap = true;
		_unconsideredAreaList = new ArrayList<>();

		if ( aPixelHeightToVerticalSpaceRatio == -1.0F ) {
			aPixelHeightToVerticalSpaceRatio = 0.2F;
		}

		if ( aPixelWidthToHorizontalSpaceRatio == -1.0F ) {
			aPixelWidthToHorizontalSpaceRatio = 0.2F;
		}

		final int theTopBorder = aTopBorder == -1 ? 0 : aTopBorder;
		final int theBottomBorder = aBottomBorder == -1 ? 0 : aBottomBorder;
		final int theLeftBorder = aLeftBorder == -1 ? 0 : aLeftBorder;
		final int theRightBorder = aRightBorder == -1 ? 0 : aRightBorder;
		int tmpRemainder;
		if ( aHorizontalSpace == -1 && aPixelWidth == -1 ) {
			tmpRemainder = ( (int) getWidth() - ( theLeftBorder + theRightBorder ) ) / aMatrixWidth;
			aHorizontalSpace = (int) ( (float) tmpRemainder * aPixelWidthToHorizontalSpaceRatio );
			if ( isForceHorizontalSpace && aHorizontalSpace == 0 ) {
				aHorizontalSpace = 1;
			}

			aPixelWidth = ( (int) getWidth() - ( theLeftBorder + theRightBorder ) ) / aMatrixWidth - aHorizontalSpace;
		}
		else if ( aHorizontalSpace == -1 && aPixelWidth != -1 ) {
			tmpRemainder = (int) getWidth() - ( theLeftBorder + theRightBorder ) - aPixelWidth * aMatrixWidth;
			aHorizontalSpace = tmpRemainder / ( aMatrixWidth - 1 );
		}
		else if ( aHorizontalSpace != -1 && aPixelWidth == -1 ) {
			tmpRemainder = (int) getWidth() - ( theLeftBorder + theRightBorder ) - aHorizontalSpace * ( aMatrixWidth - 1 );
			aPixelWidth = tmpRemainder / aMatrixWidth;
		}

		if ( aVerticalSpace == -1 && aPixelHeight == -1 ) {
			tmpRemainder = ( (int) getHeight() - ( theTopBorder + theBottomBorder ) ) / aMatrixHeight;
			aVerticalSpace = (int) ( (float) tmpRemainder * aPixelHeightToVerticalSpaceRatio );
			if ( isForceVerticalSpace && aVerticalSpace == 0 ) {
				aVerticalSpace = 1;
			}

			aPixelHeight = ( (int) getHeight() - ( theTopBorder + theBottomBorder ) ) / aMatrixHeight - aVerticalSpace;
		}
		else if ( aVerticalSpace == -1 && aPixelHeight != -1 ) {
			tmpRemainder = (int) getHeight() - ( theTopBorder + theBottomBorder ) - aPixelHeight * aMatrixHeight;
			aVerticalSpace = tmpRemainder / ( aMatrixHeight - 1 );
		}
		else if ( aVerticalSpace != -1 && aPixelHeight == -1 ) {
			tmpRemainder = (int) getHeight() - ( theTopBorder + theBottomBorder ) - aVerticalSpace * ( aMatrixHeight - 1 );
			aPixelHeight = tmpRemainder / aMatrixHeight;
		}

		_matrixWidth = aMatrixWidth;
		_matrixHeight = aMatrixHeight;
		_horizontalSpace = aHorizontalSpace;
		_verticalSpace = aVerticalSpace;
		_pixelWidth = aPixelWidth;
		_pixelHeight = aPixelHeight;
		_pixelShape = aPixelShape;
		_pixelBorderWidth = aPixelBorderWidth;
		_pixelBorderColor = aPixelBorderColor;
		if ( aLeftBorder == -1 && aRightBorder == -1 ) {
			aLeftBorder = ( (int) getWidth() - getMatrixDrawingWidth( false, true, true ) ) / 2;
			if ( aLeftBorder < 0 ) {
				aLeftBorder = 0;
			}

			aRightBorder = aLeftBorder;
		}
		else if ( aLeftBorder == -1 && aRightBorder != -1 ) {
			aLeftBorder = (int) getWidth() - getMatrixDrawingWidth( false, true, true ) - aRightBorder;
			if ( aLeftBorder < 0 ) {
				aLeftBorder = 0;
			}
		}
		else if ( aLeftBorder != -1 && aRightBorder == -1 ) {
			aRightBorder = (int) getWidth() - getMatrixDrawingWidth( false, true, true ) - aLeftBorder;
			if ( aRightBorder < 0 ) {
				aRightBorder = 0;
			}
		}

		_leftBorder = aLeftBorder;
		_rightBorder = aRightBorder;

		if ( aTopBorder == -1 && aBottomBorder == -1 ) {
			aTopBorder = ( (int) getHeight() - getMatrixDrawingHeight( false, true, true ) ) / 2;
			if ( aTopBorder < 0 ) {
				aTopBorder = 0;
			}

			aBottomBorder = aTopBorder;
		}
		else if ( aTopBorder == -1 && aBottomBorder != -1 ) {
			aTopBorder = (int) getHeight() - getMatrixDrawingHeight( false, true, true ) - aBottomBorder;
			if ( aTopBorder < 0 ) {
				aTopBorder = 0;
			}
		}
		else if ( aTopBorder != -1 && aBottomBorder == -1 ) {
			aBottomBorder = (int) getHeight() - getMatrixDrawingHeight( false, true, true ) - aTopBorder;
			if ( aBottomBorder < 0 ) {
				aBottomBorder = 0;
			}
		}

		_topBorder = aTopBorder;
		_bottomBorder = aBottomBorder;

		final int thePixelSpanWidthSum = getHorizontalSpace() * ( getMatrixWidth() - 1 );
		final int thePixelSpanHeightSum = getVerticalSpace() * ( getMatrixHeight() - 1 );
		final int thePixelWidthSum = getPixelWidth() * getMatrixWidth();
		final int thePixelHeightSum = getPixelHeight() * getMatrixHeight();
		final int theBorderWidthSum = getLeftBorder() + getRightBorder();
		final int theBorderHeightSum = getTopBorder() + getBottomBorder();
		final int theWidth = thePixelSpanWidthSum + thePixelWidthSum + theBorderWidthSum;
		final int theHeight = thePixelSpanHeightSum + thePixelHeightSum + theBorderHeightSum;
		final java.awt.Dimension theDimension = new java.awt.Dimension( theWidth, theHeight );
		setPrefSize( theDimension.getWidth(), theDimension.getHeight() );
		setMinSize( theDimension.getWidth(), theDimension.getHeight() );
		setWidth( theDimension.getWidth() );
		setHeight( theDimension.getHeight() );

		_rgbPixmapBuilder = new RgbPixmapBuilderImpl( aMatrixWidth, aMatrixHeight );

		if ( aBackgroundColor != null ) {
			final javafx.scene.shape.Rectangle theBackground = new javafx.scene.shape.Rectangle( 0, 0, theDimension.getWidth(), theDimension.getHeight() );
			theBackground.setFill( aBackgroundColor );
			getChildren().add( theBackground );
		}

		_shapes = new Shape[aMatrixWidth][aMatrixHeight];
		int eLeft;
		int eTop;
		for ( int x = 0; x < aMatrixWidth; x++ ) {
			for ( int y = 0; y < aMatrixHeight; y++ ) {
				eLeft = _leftBorder + x * _horizontalSpace + x * _pixelWidth;
				eTop = _topBorder + y * _verticalSpace + y * _pixelHeight;
				switch ( _pixelShape ) {
				case ELLIPSE -> {
					final Ellipse theEllipse = new Ellipse( eLeft + _pixelWidth / 2, eTop + _pixelHeight / 2, _pixelWidth / 2, _pixelHeight / 2 );
					theEllipse.setFill( _inactivePixelColor );
					if ( _pixelBorderColor != null && _pixelBorderWidth > 0 ) {
						theEllipse.setStroke( _pixelBorderColor );
						theEllipse.setStrokeWidth( _pixelBorderWidth );
						theEllipse.setStrokeLineCap( StrokeLineCap.ROUND );
						theEllipse.setStrokeLineJoin( StrokeLineJoin.MITER );
					}
					_shapes[x][y] = theEllipse;
				}
				case RECTANGLE -> {
					final javafx.scene.shape.Rectangle theRectangle = new javafx.scene.shape.Rectangle( eLeft, eTop, _pixelWidth, _pixelHeight );
					theRectangle.setFill( _inactivePixelColor );
					if ( _pixelBorderColor != null && _pixelBorderWidth > 0 ) {
						theRectangle.setStroke( _pixelBorderColor );
						theRectangle.setStrokeWidth( _pixelBorderWidth );
						theRectangle.setStrokeLineCap( StrokeLineCap.SQUARE );
						theRectangle.setStrokeLineJoin( StrokeLineJoin.MITER );
					}
					_shapes[x][y] = theRectangle;
				}
				default -> throw new UnhandledEnumBugException( _pixelShape );
				}

				getChildren().add( _shapes[x][y] );
				_rgbPixmapBuilder.setPixelAt( toRgbPixel( _inactivePixelColor ), x, y );
			}
		}
	}

	/**
	 * Gets the {@link Color} at the given position.
	 *
	 * @param aXPos the a X pos
	 * @param aYPos the a Y pos
	 * 
	 * @return the {@link Color} at the given position
	 * 
	 * @throws IndexOutOfBoundsException the index out of bounds exception
	 */
	public Color getColorAt( int aXPos, int aYPos ) {
		return toFxColor( _rgbPixmapBuilder.getColorAt( aXPos, aYPos ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixel getPixelAt( int aXPos, int aYPos ) {
		return _rgbPixmapBuilder.getPixelAt( aXPos, aYPos );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getTopBorder() {
		return _topBorder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getBottomBorder() {
		return _bottomBorder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getLeftBorder() {
		return _leftBorder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getRightBorder() {
		return _rightBorder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getHorizontalSpace() {
		return _horizontalSpace;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getVerticalSpace() {
		return _verticalSpace;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPixelWidth() {
		return _pixelWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPixelHeight() {
		return _pixelHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMatrixDrawingWidth( boolean isWithBorder, boolean isWithPixelSpace, boolean isWithPixelWidth ) {
		final int theBorder = isWithBorder ? getLeftBorder() + getRightBorder() : 0;
		final int theSpace = isWithPixelSpace ? getHorizontalSpace() : 0;
		final int thePixelWidth = isWithPixelWidth ? getPixelWidth() : 0;
		return theBorder * 2 + theSpace * ( getMatrixWidth() - 1 ) + thePixelWidth * getMatrixWidth();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMatrixDrawingHeight( boolean isWithBorder, boolean isWithPixelSpace, boolean isWithPixelHeight ) {
		final int theBorder = isWithBorder ? getTopBorder() + getBottomBorder() : 0;
		final int theSpace = isWithPixelSpace ? getVerticalSpace() : 0;
		final int thePixelHeight = isWithPixelHeight ? getPixelWidth() : 0;
		return theBorder * 2 + theSpace * ( getMatrixHeight() - 1 ) + thePixelHeight * getMatrixHeight();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapBuilder getRgbPixmap() {
		return _rgbPixmapBuilder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPixmapOffsetX() {
		return _pixmapOffsetX;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPixmapOffsetY() {
		return _pixmapOffsetY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isHorizontalWrapEnabled() {
		return _isHorizontalWrap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isVerticalWrapEnabled() {
		return _isVerticalWrap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMatrixWidth() {
		return _matrixWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getMatrixHeight() {
		return _matrixHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Dimension getMatrixSize() {
		return new DimensionImpl( _matrixWidth, _matrixHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbColor getInactivePixelColor() {
		return toRgbPixel( _inactivePixelColor );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		for ( int x = 0; x < getMatrixWidth(); ++x ) {
			for ( int y = 0; y < getMatrixHeight(); ++y ) {
				_rgbPixmapBuilder.setPixelAt( toRgbPixel( _inactivePixelColor ), x, y );
			}
		}
		repaint();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PixelShape getPixelShape() {
		return _pixelShape;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addBlankArea( int aXPosition, int aYPosition, int aWidth, int aHeight ) {
		final Rectangle theRectangle = new RectangleImpl( aXPosition, aYPosition, aWidth, aHeight );
		return addBlankArea( theRectangle );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean addBlankArea( Rectangle aRectangle ) {
		synchronized ( _unconsideredAreaList ) {
			return !_unconsideredAreaList.contains( aRectangle ) ? _unconsideredAreaList.add( aRectangle ) : false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean removeBlankArea( Rectangle aRectangle ) {
		synchronized ( _unconsideredAreaList ) {
			return _unconsideredAreaList.remove( aRectangle );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clearBlankAreas() {
		synchronized ( _unconsideredAreaList ) {
			_unconsideredAreaList.clear();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public RgbPixmapBuilder getPixmapSnapshot() {
		final RgbPixmapBuilder theMatrix = new RgbPixmapBuilderImpl( getMatrixWidth(), getMatrixHeight() );
		theMatrix.mergeWith( _rgbPixmapBuilder, _pixmapOffsetX, _pixmapOffsetY, _isHorizontalWrap, _isVerticalWrap, 0, 0, _matrixWidth, _matrixHeight );
		return theMatrix;
	}

	private boolean isPaintPixelAt( int aXPosition, int aYPosition ) {
		if ( _unconsideredAreaList.isEmpty() ) {
			return true;
		}
		else {
			final Iterator<Rectangle> e = _unconsideredAreaList.iterator();

			Rectangle eRectangle;
			do {
				if ( !e.hasNext() ) {
					return true;
				}

				eRectangle = e.next();
			} while ( aXPosition < eRectangle.getPositionX() || aXPosition >= eRectangle.getPositionX() + eRectangle.getWidth() || aYPosition < eRectangle.getPositionY() || aYPosition >= eRectangle.getPositionY() + eRectangle.getHeight() );

			return false;
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPixmapOffset( int aPosX, int aPosY ) {
		if ( aPosX < 0 ) {
			throw new IllegalArgumentException( "Argument must be greater than or equal to <0>! You provided an x-offset of < " + aPosX + ">" );
		}
		else if ( aPosY < 0 ) {
			throw new IllegalArgumentException( "Argument must be greater than or equal to <0>! You provided an y-offset of < " + aPosY + ">" );
		}
		else {
			this._pixmapOffsetX = aPosX;
			this._pixmapOffsetY = aPosY;
		}
	}

	/**
	 * Paints the {@link Color} at the given position.
	 *
	 * @param aColor the {@link Color} for the pixel at the given position.
	 * @param aPosX the x position
	 * @param aPosY the y position
	 */
	public void setColorAt( Color aColor, int aPosX, int aPosY ) {
		if ( aPosX < _matrixWidth && aPosY < _matrixHeight ) {
			_rgbPixmapBuilder.setPixelAt( toRgbPixel( aColor ), aPosX, aPosY );
			if ( Platform.isFxApplicationThread() ) {
				_shapes[aPosX][aPosY].setFill( aColor );
			}
			else {
				Platform.runLater( () -> _shapes[aPosX][aPosY].setFill( aColor ) );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setPixelAtAt( RgbPixel aPixel, int aPosX, int aPosY ) {
		if ( aPosX < _matrixWidth && aPosY < _matrixHeight ) {
			_rgbPixmapBuilder.setPixelAt( aPixel, aPosX, aPosY );
			if ( Platform.isFxApplicationThread() ) {
				_shapes[aPosX][aPosY].setFill( toFxColor( aPixel ) );
			}
			else {
				Platform.runLater( () -> _shapes[aPosX][aPosY].setFill( toFxColor( aPixel ) ) );
			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void repaint() {
		for ( int x = 0; x < _matrixWidth; ++x ) {
			for ( int y = 0; y < _matrixHeight; ++y ) {
				if ( isPaintPixelAt( x, y ) ) {
					int eX = x + _pixmapOffsetX;
					int eY = y + _pixmapOffsetY;
					final boolean isHorizontalWrapSet;
					if ( eX >= _rgbPixmapBuilder.getWidth() ) {
						isHorizontalWrapSet = true;
						eX -= _rgbPixmapBuilder.getWidth();
					}
					else {
						isHorizontalWrapSet = false;
					}

					final boolean isVerticalWrapSet;
					if ( eY >= _rgbPixmapBuilder.getHeight() ) {
						isVerticalWrapSet = true;
						eY -= _rgbPixmapBuilder.getHeight();
					}
					else {
						isVerticalWrapSet = false;
					}

					final Color eColor;
					if ( ( !isHorizontalWrapSet || isHorizontalWrapEnabled() ) && ( !isVerticalWrapSet || isVerticalWrapEnabled() ) ) {
						eColor = toFxColor( _rgbPixmapBuilder.getColorAt( eX, eY ) );
					}
					else {
						eColor = _inactivePixelColor;
					}
					if ( Platform.isFxApplicationThread() ) {
						_shapes[x][y].setFill( eColor );
					}
					else {
						final int theX = x;
						final int theY = y;
						Platform.runLater( () -> _shapes[theX][theY].setFill( eColor ) );
					}
				}
			}
		}
	}

	protected static Color toFxColor( RgbPixel aPixel ) {
		Color fxColor = _pixelToFxColorCache.get( aPixel );
		if ( fxColor == null ) {
			final int r = aPixel.getRed();
			final int g = aPixel.getGreen();
			final int b = aPixel.getBlue();
			final int a = aPixel.getAlpha();
			final double opacity = a / 255.0;
			fxColor = Color.rgb( r, g, b, opacity );
			_pixelToFxColorCache.put( aPixel, fxColor );
		}
		return fxColor;
	}

	protected static Color toFxColor( java.awt.Color aAwtColor ) {
		final int r = aAwtColor.getRed();
		final int g = aAwtColor.getGreen();
		final int b = aAwtColor.getBlue();
		final int a = aAwtColor.getAlpha();
		final double opacity = a / 255.0;
		final Color fxColor = Color.rgb( r, g, b, opacity );
		return fxColor;
	}

	protected static Color toFxColor( String aWebColor ) {
		return Color.web( aWebColor );
	}

	protected static RgbPixel toRgbPixel( Color aFxColor ) {
		RgbPixel pixel = _fxColorToPixelCache.get( aFxColor );
		if ( pixel == null ) {
			pixel = new RgbPixelImpl( new java.awt.Color( (float) aFxColor.getRed(), (float) aFxColor.getGreen(), (float) aFxColor.getBlue(), (float) aFxColor.getOpacity() ) );
			_fxColorToPixelCache.put( aFxColor, pixel );
		}
		return pixel;
	}
}
