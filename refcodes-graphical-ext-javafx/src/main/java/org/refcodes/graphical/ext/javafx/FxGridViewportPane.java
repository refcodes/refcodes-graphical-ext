// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical.ext.javafx;

import org.refcodes.graphical.Dimension;
import org.refcodes.graphical.FieldDimension;
import org.refcodes.graphical.GridMode;
import org.refcodes.graphical.GridViewportPane;
import org.refcodes.graphical.MoveMode;
import org.refcodes.graphical.Offset;
import org.refcodes.graphical.Position;
import org.refcodes.graphical.ViewportDimension;
import org.refcodes.graphical.ViewportOffset;

import javafx.collections.ObservableList;

/**
 * The {@link FxGridViewportPane} is a ready to use implementation of the
 * {@link GridViewportPane} interface extending the
 * {@link AbstractFxGridViewportPane} class.
 * 
 * Use {@link ObservableList#add(Object)} of the {@link #getChildren()} method
 * to add sprites. You may want to make sure the initial sprites' position
 * matches the grid.
 */
public class FxGridViewportPane extends AbstractFxGridViewportPane<FxGridViewportPane> {

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportOffsetX( int aOffsetX ) {
		setViewportOffsetX( aOffsetX );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportOffsetY( int aOffsetY ) {
		setViewportOffsetY( aOffsetY );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportOffset( int aOffsetX, int aOffsetY ) {
		setViewportOffset( aOffsetX, aOffsetY );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportOffset( ViewportOffset aOffset ) {
		setViewportOffset( aOffset );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportOffset( Offset aOffset ) {
		setViewportOffset( aOffset.getOffsetX(), aOffset.getOffsetY() );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportOffset( Position aOffset ) {
		setViewportOffset( aOffset );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportWidth( int aWidth ) {
		setViewportWidth( aWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportHeight( int aHeight ) {
		setViewportHeight( aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportDimension( int aWidth, int aHeight ) {
		setViewportDimension( aWidth, aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportDimension( ViewportDimension aDimension ) {
		setViewportDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withViewportDimension( Dimension aDimension ) {
		setViewportDimension( aDimension.getWidth(), aDimension.getHeight() );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withFieldWidth( int aWidth ) {
		setFieldWidth( aWidth );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withFieldHeight( int aHeight ) {
		setFieldHeight( aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withFieldDimension( int aWidth, int aHeight ) {
		setFieldDimension( aWidth, aHeight );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withFieldDimension( int aWidth, int aHeight, int aGap ) {
		setFieldDimension( aWidth, aHeight, aGap );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withFieldDimension( FieldDimension aDimension ) {
		setFieldDimension( aDimension );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withFieldDimension( Dimension aDimension ) {
		setFieldDimension( aDimension.getWidth(), aDimension.getHeight() );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withFieldGap( int aGap ) {
		setFieldGap( aGap );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withDragOpacity( double aOpacity ) {
		setDragOpacity( aOpacity );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withMoveMode( MoveMode aMoveMode ) {
		setMoveMode( aMoveMode );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public FxGridViewportPane withGridMode( GridMode aGridMode ) {
		setGridMode( aGridMode );
		return this;
	}
}
