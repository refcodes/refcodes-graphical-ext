package org.refcodes.graphical.ext.javafx;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.refcodes.graphical.PixGridBannerPanel;
import org.refcodes.graphical.PixelShape;
import org.refcodes.graphical.Position;
import org.refcodes.graphical.PositionImpl;
import org.refcodes.graphical.RgbPixmap.RgbPixmapBuilder;

import javafx.scene.paint.Color;

public class FxPixGridBannerPanel extends FxPixGridMatrixPane implements PixGridBannerPanel {

	private static final Random RND = new Random();

	/**
	 * {@inheritDoc}
	 */
	public FxPixGridBannerPanel( int aMatrixWidth, int aMatrixHeight, Color aBackgroundColor, int aPixelWidth, int aPixelHeight, PixelShape aPixelShape, float aPixelBorderWidth, Color aPixelBorderColor, Color aInactivePixelColor, int aTopBorder, int aBottomBorder, int aLeftBorder, int aRightBorder, int aHorizontalSpace, float aPixelWidthToHorizontalSpaceRatio, boolean aIsForceHorizontalSpace, int aVerticalSpace, float aPixelHeightToVerticalSpaceRatio, boolean aIsForceVerticalSpace ) {
		super( aMatrixWidth, aMatrixHeight, aBackgroundColor, aPixelWidth, aPixelHeight, aPixelShape, aPixelBorderWidth, aPixelBorderColor, aInactivePixelColor, aTopBorder, aBottomBorder, aLeftBorder, aRightBorder, aHorizontalSpace, aPixelWidthToHorizontalSpaceRatio, aIsForceHorizontalSpace, aVerticalSpace, aPixelHeightToVerticalSpaceRatio, aIsForceVerticalSpace );
	}

	/**
	 * {@inheritDoc}
	 */
	public FxPixGridBannerPanel( int aMatrixWidth, int aMatrixHeight, java.awt.Color aBackgroundColor, int aPixelWidth, int aPixelHeight, PixelShape aPixelShape, float aPixelBorderWidth, java.awt.Color aPixelBorderColor, java.awt.Color aInactivePixelColor, int aTopBorder, int aBottomBorder, int aLeftBorder, int aRightBorder, int aHorizontalSpace, float aPixelWidthToHorizontalSpaceRatio, boolean isForceHorizontalSpace, int aVerticalSpace, float aPixelHeightToVerticalSpaceRatio, boolean isForceVerticalSpace ) {
		super( aMatrixWidth, aMatrixHeight, aBackgroundColor, aPixelWidth, aPixelHeight, aPixelShape, aPixelBorderWidth, aPixelBorderColor, aInactivePixelColor, aTopBorder, aBottomBorder, aLeftBorder, aRightBorder, aHorizontalSpace, aPixelWidthToHorizontalSpaceRatio, isForceHorizontalSpace, aVerticalSpace, aPixelHeightToVerticalSpaceRatio, isForceVerticalSpace );
	}

	/**
	 * {@inheritDoc}
	 */
	public FxPixGridBannerPanel( int aMatrixWidth, int aMatrixHeight, String aBackgroundColor, int aPixelWidth, int aPixelHeight, PixelShape aPixelShape, float aPixelBorderWidth, String aPixelBorderColor, String aInactivePixelColor, int aTopBorder, int aBottomBorder, int aLeftBorder, int aRightBorder, int aHorizontalSpace, float aPixelWidthToHorizontalSpaceRatio, boolean isForceHorizontalSpace, int aVerticalSpace, float aPixelHeightToVerticalSpaceRatio, boolean isForceVerticalSpace ) {
		super( aMatrixWidth, aMatrixHeight, aBackgroundColor, aPixelWidth, aPixelHeight, aPixelShape, aPixelBorderWidth, aPixelBorderColor, aInactivePixelColor, aTopBorder, aBottomBorder, aLeftBorder, aRightBorder, aHorizontalSpace, aPixelWidthToHorizontalSpaceRatio, isForceHorizontalSpace, aVerticalSpace, aPixelHeightToVerticalSpaceRatio, isForceVerticalSpace );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveNorth( int aSteps, int aStartDelayMillis, int aEndDelayMillis ) {
		if ( getRgbPixmap() != null ) {
			final int theXOffset = getPixmapOffsetX();
			if ( aSteps <= 0 ) {
				throw new IllegalArgumentException( "You provided an illegal value for the steps argument! You provided the value <" + aSteps + ">!" );
			}
			else {
				int eYOffset = getPixmapOffsetY();
				final float theDelaySteps;
				if ( aStartDelayMillis < aEndDelayMillis ) {
					theDelaySteps = (float) ( ( aEndDelayMillis - aStartDelayMillis ) / aSteps );
				}
				else {
					theDelaySteps = (float) ( ( aStartDelayMillis - aEndDelayMillis ) / aSteps * -1 );
				}

				float eDelay = (float) aStartDelayMillis;

				for ( int i = 0; i < aSteps; ++i ) {
					--eYOffset;
					if ( eYOffset < 0 ) {
						eYOffset += getRgbPixmap().getHeight() - 1;
					}

					setPixmapOffset( theXOffset, eYOffset );

					repaint();

					delay( (int) eDelay );

					eDelay += theDelaySteps;
					if ( eDelay < 0.0F ) {
						eDelay = 0.0F;
					}
				}

			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveNorthEast( int aSteps, int aStartDelayMillis, int aEndDelayMillis ) {
		if ( getRgbPixmap() != null ) {
			int eXOffset = getPixmapOffsetX();
			if ( aSteps <= 0 ) {
				throw new IllegalArgumentException( "You provided an illegal value for the steps argument! You provided the value <" + aSteps + ">!" );
			}
			else {
				int eYOffset = getPixmapOffsetY();
				final float theDelaySteps;
				if ( aStartDelayMillis < aEndDelayMillis ) {
					theDelaySteps = (float) ( ( aEndDelayMillis - aStartDelayMillis ) / aSteps );
				}
				else {
					theDelaySteps = (float) ( ( aStartDelayMillis - aEndDelayMillis ) / aSteps * -1 );
				}

				float eDelay = (float) aStartDelayMillis;

				for ( int i = 0; i < aSteps; ++i ) {
					++eXOffset;
					if ( eXOffset >= getRgbPixmap().getWidth() ) {
						eXOffset -= getRgbPixmap().getWidth() - 1;
					}

					--eYOffset;
					if ( eYOffset < 0 ) {
						eYOffset += getRgbPixmap().getHeight() - 1;
					}

					setPixmapOffset( eXOffset, eYOffset );

					repaint();
					delay( (int) eDelay );

					eDelay += theDelaySteps;
					if ( eDelay < 0.0F ) {
						eDelay = 0.0F;
					}
				}

			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveEast( int aSteps, int aStartDelayMillis, int aEndDelayMillis ) {
		if ( getRgbPixmap() != null ) {
			if ( aSteps <= 0 ) {
				throw new IllegalArgumentException( "You provided an illegal value for the steps argument! You provided the value <" + aSteps + ">!" );
			}
			else {
				final int theYOffset = getPixmapOffsetY();
				int eXOffset = getPixmapOffsetX();
				final float theDelaySteps;
				if ( aStartDelayMillis < aEndDelayMillis ) {
					theDelaySteps = (float) ( ( aEndDelayMillis - aStartDelayMillis ) / aSteps );
				}
				else {
					theDelaySteps = (float) ( ( aStartDelayMillis - aEndDelayMillis ) / aSteps * -1 );
				}

				float eDelay = (float) aStartDelayMillis;

				for ( int i = 0; i < aSteps; ++i ) {
					++eXOffset;
					if ( eXOffset >= getRgbPixmap().getWidth() ) {
						eXOffset -= getRgbPixmap().getWidth() - 1;
					}

					setPixmapOffset( eXOffset, theYOffset );

					repaint();
					delay( (int) eDelay );

					eDelay += theDelaySteps;
					if ( eDelay < 0.0F ) {
						eDelay = 0.0F;
					}
				}

			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveSouthEast( int aSteps, int aStartDelayMillis, int aEndDelayMillis ) {
		if ( getRgbPixmap() != null ) {
			int eXOffset = getPixmapOffsetX();
			if ( aSteps <= 0 ) {
				throw new IllegalArgumentException( "You provided an illegal value for the steps argument! You provided the value <" + aSteps + ">!" );
			}
			else {
				int eYOffset = getPixmapOffsetY();
				final float theDelaySteps;
				if ( aStartDelayMillis < aEndDelayMillis ) {
					theDelaySteps = (float) ( ( aEndDelayMillis - aStartDelayMillis ) / aSteps );
				}
				else {
					theDelaySteps = (float) ( ( aStartDelayMillis - aEndDelayMillis ) / aSteps * -1 );
				}

				float eDelay = (float) aStartDelayMillis;

				for ( int i = 0; i < aSteps; ++i ) {
					++eXOffset;
					if ( eXOffset >= getRgbPixmap().getWidth() ) {
						eXOffset -= getRgbPixmap().getWidth() - 1;
					}

					++eYOffset;
					if ( eYOffset >= getRgbPixmap().getHeight() ) {
						eYOffset -= getRgbPixmap().getHeight() - 1;
					}

					setPixmapOffset( eXOffset, eYOffset );

					repaint();
					delay( (int) eDelay );

					eDelay += theDelaySteps;
					if ( eDelay < 0.0F ) {
						eDelay = 0.0F;
					}
				}

			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveSouth( int aSteps, int aStartDelayMillis, int aEndDelayMillis ) {
		if ( getRgbPixmap() != null ) {
			final int theXOffset = getPixmapOffsetX();
			if ( aSteps <= 0 ) {
				throw new IllegalArgumentException( "You provided an illegal value for the steps argument! You provided the value <" + aSteps + ">!" );
			}
			else {
				int eYOffset = getPixmapOffsetY();
				final float theDelaySteps;
				if ( aStartDelayMillis < aEndDelayMillis ) {
					theDelaySteps = (float) ( ( aEndDelayMillis - aStartDelayMillis ) / aSteps );
				}
				else {
					theDelaySteps = (float) ( ( aStartDelayMillis - aEndDelayMillis ) / aSteps * -1 );
				}

				float eDelay = (float) aStartDelayMillis;

				for ( int i = 0; i < aSteps; ++i ) {
					++eYOffset;
					if ( eYOffset >= getRgbPixmap().getHeight() ) {
						eYOffset -= getRgbPixmap().getHeight() - 1;
					}

					setPixmapOffset( theXOffset, eYOffset );

					repaint();
					delay( (int) eDelay );

					eDelay += theDelaySteps;
					if ( eDelay < 0.0F ) {
						eDelay = 0.0F;
					}
				}

			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveSouthWest( int aSteps, int aStartDelayMillis, int aEndDelayMillis ) {
		if ( getRgbPixmap() != null ) {
			int eXOffset = getPixmapOffsetX();
			if ( aSteps <= 0 ) {
				throw new IllegalArgumentException( "You provided an illegal value for the steps argument! You provided the value <" + aSteps + ">!" );
			}
			else {
				int eYOffset = getPixmapOffsetY();
				final float theDelaySteps;
				if ( aStartDelayMillis < aEndDelayMillis ) {
					theDelaySteps = (float) ( ( aEndDelayMillis - aStartDelayMillis ) / aSteps );
				}
				else {
					theDelaySteps = (float) ( ( aStartDelayMillis - aEndDelayMillis ) / aSteps * -1 );
				}

				float eDelay = (float) aStartDelayMillis;

				for ( int i = 0; i < aSteps; ++i ) {
					--eXOffset;
					if ( eXOffset < 0 ) {
						eXOffset += getRgbPixmap().getWidth() - 1;
					}

					++eYOffset;
					if ( eYOffset >= getRgbPixmap().getHeight() ) {
						eYOffset -= getRgbPixmap().getHeight() - 1;
					}

					setPixmapOffset( eXOffset, eYOffset );

					repaint();
					delay( (int) eDelay );

					eDelay += theDelaySteps;
					if ( eDelay < 0.0F ) {
						eDelay = 0.0F;
					}
				}

			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveWest( int aSteps, int aStartDelayMillis, int aEndDelayMillis ) {
		if ( getRgbPixmap() != null ) {
			if ( aSteps <= 0 ) {
				throw new IllegalArgumentException( "You provided an illegal value for the steps argument! You provided the value <" + aSteps + ">!" );
			}
			else {
				final int theYOffset = getPixmapOffsetY();
				int eXOffset = getPixmapOffsetX();
				final float theDelaySteps;
				if ( aStartDelayMillis < aEndDelayMillis ) {
					theDelaySteps = (float) ( ( aEndDelayMillis - aStartDelayMillis ) / aSteps );
				}
				else {
					theDelaySteps = (float) ( ( aStartDelayMillis - aEndDelayMillis ) / aSteps * -1 );
				}

				float eDelay = (float) aStartDelayMillis;

				for ( int i = 0; i < aSteps; ++i ) {
					--eXOffset;
					if ( eXOffset < 0 ) {
						eXOffset += getRgbPixmap().getWidth() - 1;
					}

					setPixmapOffset( eXOffset, theYOffset );

					repaint();
					delay( (int) eDelay );

					eDelay += theDelaySteps;
					if ( eDelay < 0.0F ) {
						eDelay = 0.0F;
					}
				}

			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void moveNorthWest( int aSteps, int aStartDelayMillis, int aEndDelayMillis ) {
		if ( getRgbPixmap() != null ) {
			int eXOffset = getPixmapOffsetX();
			if ( aSteps <= 0 ) {
				throw new IllegalArgumentException( "You provided an illegal value for the steps argument! You provided the value <" + aSteps + ">!" );
			}
			else {
				int eYOffset = getPixmapOffsetY();
				final float theDelaySteps;
				if ( aStartDelayMillis < aEndDelayMillis ) {
					theDelaySteps = (float) ( ( aEndDelayMillis - aStartDelayMillis ) / aSteps );
				}
				else {
					theDelaySteps = (float) ( ( aStartDelayMillis - aEndDelayMillis ) / aSteps * -1 );
				}

				float eDelay = (float) aStartDelayMillis;

				for ( int i = 0; i < aSteps; ++i ) {
					--eXOffset;
					if ( eXOffset < 0 ) {
						eXOffset += getRgbPixmap().getWidth() - 1;
					}

					--eYOffset;
					if ( eYOffset < 0 ) {
						eYOffset += getRgbPixmap().getHeight() - 1;
					}

					setPixmapOffset( eXOffset, eYOffset );
					setPixmapOffset( eXOffset, eYOffset );

					repaint();
					delay( (int) eDelay );

					eDelay += theDelaySteps;
					if ( eDelay < 0.0F ) {
						eDelay = 0.0F;
					}
				}

			}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void stepFadeToPixmap( RgbPixmapBuilder aFadeToColorMatrix, int aOffsetX, int aOffsetY, int aReplaceStepWidth, int aDelayMilliSec ) {
		final int theMatrixSize = getMatrixWidth() * getMatrixHeight();
		if ( aReplaceStepWidth > 0 && aReplaceStepWidth < theMatrixSize ) {
			_rgbPixmapBuilder = getPixmapSnapshot();
			setPixmapOffset( 0, 0 );

			final int theWidth = getMatrixWidth();
			// int theHeight = getMatrixHeight();
			int eIndex = 0;
			int eDrawn = 0;
			final List<Position> theDrawnList = new ArrayList<>();

			while ( eDrawn < theMatrixSize ) {
				int eYPos = eIndex / theWidth;
				int eXPos = eIndex - eYPos * theWidth;

				Position ePoint;
				for ( ePoint = new PositionImpl( eXPos, eYPos ); theDrawnList.contains( ePoint ); ePoint = new PositionImpl( eXPos, eYPos ) ) {
					++eIndex;
					eYPos = eIndex / theWidth;
					eXPos = eIndex - eYPos * theWidth;
				}

				theDrawnList.add( ePoint );

				paintPixelAt( aFadeToColorMatrix, eYPos, eXPos, aOffsetX, aOffsetY, _inactivePixelColor );

				eIndex += aReplaceStepWidth;
				if ( eIndex >= theMatrixSize ) {
					eIndex -= theMatrixSize;
				}

				++eDrawn;
				delay( aDelayMilliSec );
			}

			for ( int x = 0; x < _rgbPixmapBuilder.getWidth(); ++x ) {
				for ( int y = 0; y < _rgbPixmapBuilder.getHeight(); ++y ) {
					paintPixelAt( aFadeToColorMatrix, x, y, aOffsetX, aOffsetY, _inactivePixelColor );
				}
			}

			theDrawnList.clear();
			_rgbPixmapBuilder = aFadeToColorMatrix;
			setPixmapOffset( aOffsetX, aOffsetY );
			return;
		}
		else {
			throw new IllegalArgumentException( "The provided replace step with in argument <aReplaceStepWidth> must be smaller than the number of matrix pixels and greater than 0! You provided a step with of <" + aReplaceStepWidth + ">, the size of matrix is <" + theMatrixSize + "> pixels." );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void rndFadeToPixmap( RgbPixmapBuilder aFadeToColorMatrix, int aOffsetX, int aOffsetY, float aPixelPropability, int aDelayMilliSec ) {
		final int theMatrixSize = getMatrixWidth() * getMatrixHeight();
		final RgbPixmapBuilder theTempMatrix = getPixmapSnapshot();
		_rgbPixmapBuilder = theTempMatrix;
		setPixmapOffset( 0, 0 );
		//	int theWidth = getMatrixWidth();
		//	int theHeight = getMatrixHeight();
		int eDrawn = 0;
		int thePixelNotDrawnCount = 0;
		final List<Position> theDrawnList = new ArrayList<>();

		int y;
		int x;
		while ( eDrawn < theMatrixSize && thePixelNotDrawnCount < 5 ) {
			boolean eIsPixelDrawn = false;

			for ( y = 0; y < getMatrixHeight(); ++y ) {
				for ( x = 0; x < getMatrixWidth(); ++x ) {
					final Position ePoint = new PositionImpl( x, y );
					final float eRandomNumber;
					if ( !theDrawnList.contains( ePoint ) ) {
						eRandomNumber = RND.nextFloat();
						if ( (double) eRandomNumber <= aPixelPropability ) {
							theDrawnList.add( ePoint );
							paintPixelAt( aFadeToColorMatrix, y, x, aOffsetX, aOffsetY, _inactivePixelColor );
							++eDrawn;
							eIsPixelDrawn = true;
							delay( aDelayMilliSec );
						}
					}
				}
			}

			if ( !eIsPixelDrawn ) {
				++thePixelNotDrawnCount;
			}
			else {
				thePixelNotDrawnCount = 0;
			}
		}

		for ( y = 0; y < theTempMatrix.getHeight(); ++y ) {
			for ( x = 0; x < theTempMatrix.getWidth(); ++x ) {
				paintPixelAt( aFadeToColorMatrix, x, y, aOffsetX, aOffsetY, _inactivePixelColor );
			}
		}

		theDrawnList.clear();
		_rgbPixmapBuilder = aFadeToColorMatrix;
		setPixmapOffset( aOffsetX, aOffsetY );
	}

	protected void paintPixelAt( RgbPixmapBuilder aFadeToColorMatrix, int aPosY, int aPosX, int aOffsetX, int aOffsetY, Color aInactiveColor ) {
		int eXPosFadeTo = aPosX + aOffsetX;
		int eYPosFadeTo = aPosY + aOffsetY;
		if ( eXPosFadeTo >= aFadeToColorMatrix.getWidth() && isHorizontalWrapEnabled() ) {
			eXPosFadeTo -= aFadeToColorMatrix.getWidth();
		}

		if ( eYPosFadeTo >= aFadeToColorMatrix.getHeight() && isVerticalWrapEnabled() ) {
			eYPosFadeTo -= aFadeToColorMatrix.getHeight();
		}

		if ( eXPosFadeTo < aFadeToColorMatrix.getWidth() && eYPosFadeTo < aFadeToColorMatrix.getHeight() ) {
			aInactiveColor = toFxColor( aFadeToColorMatrix.getColorAt( eXPosFadeTo, eYPosFadeTo ) );
		}
		setColorAt( aInactiveColor, aPosX, aPosY );
	}

	private void delay( long aDelayMilliSec ) {
		try {
			Thread.sleep( aDelayMilliSec );
		}
		catch ( InterruptedException var26 ) {}
	}
}
