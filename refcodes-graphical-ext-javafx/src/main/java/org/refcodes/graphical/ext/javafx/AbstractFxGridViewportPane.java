// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical.ext.javafx;

import java.util.function.BiConsumer;

import org.refcodes.graphical.Dimension;
import org.refcodes.graphical.FieldDimension;
import org.refcodes.graphical.GridMode;
import org.refcodes.graphical.GridViewportPane;
import org.refcodes.graphical.MoveMode;
import org.refcodes.graphical.Offset;
import org.refcodes.graphical.Opacity;
import org.refcodes.graphical.Position;
import org.refcodes.graphical.ViewportDimension;
import org.refcodes.graphical.ViewportOffset;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;

/**
 * The {@link AbstractFxGridViewportPane} is an abstract implementation of the
 * {@link GridViewportPane} interface.
 * 
 * Use the {@link ObservableList#add(Object)} method of {@link #getChildren()}
 * to add sprites. You may want to make sure the initial sprites' position
 * matches the grid.
 */
public abstract class AbstractFxGridViewportPane<B extends AbstractFxGridViewportPane<B>> extends Pane implements GridViewportPane<Node, B> { // ,PaddingProperty, PaddingBuilder<AbstractFxGridViewportPane<B>>

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int DEFAULT_MOVE_VIEWPORT_DURATION_MILLIS = 250;
	private static final int DEFAULT_DRAG_VIEWPORT_DURATION_MILLIS = 250;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private FxGridDragSpriteEventHandler _dragEventHandler = null;
	private int _viewportWidth;
	private int _viewportHeight;
	private int _viewportOffsetY;
	private int _viewportOffsetX;
	private int _fieldWidth;
	private int _fieldHeight;
	private int _fieldGap;
	private double _dragOpacity = Opacity.DRAG.getOpacity();
	private GridMode _gridMode = GridMode.NONE;
	private boolean _hasContent = false;
	private MoveMode _moveMode = MoveMode.SMOOTH;
	private int _moveViewportDurationMillis = DEFAULT_MOVE_VIEWPORT_DURATION_MILLIS;
	private int _dragViewportDurationMillis = DEFAULT_DRAG_VIEWPORT_DURATION_MILLIS;
	private BiConsumer<Integer, Integer> _mouseClickedListener;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Default constructor of the {@link AbstractFxGridViewportPane}.
	 */
	public AbstractFxGridViewportPane() {}

	/**
	 * Constructor of the {@link AbstractFxGridViewportPane} notifying the
	 * provided listener upon mouser clicks (alongside the position in the grid
	 * of the click).
	 * 
	 * @param aMouseClickedListener The listener being provided with the grid
	 *        positions of where the mouse was clicked.
	 */
	public AbstractFxGridViewportPane( BiConsumer<Integer, Integer> aMouseClickedListener ) {
		_mouseClickedListener = aMouseClickedListener;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportOffsetX() {
		return _viewportOffsetY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportOffsetY() {
		return _viewportOffsetY;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffsetX( int aOffsetX ) {
		setViewportOffset( aOffsetX, _viewportOffsetY );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffsetY( int aOffsetY ) {
		setViewportOffset( _viewportOffsetX, aOffsetY );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffset( ViewportOffset aOffset ) {
		setViewportOffset( aOffset.getViewportOffsetX(), aOffset.getViewportOffsetY() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffset( Offset aOffset ) {
		setViewportOffset( aOffset.getOffsetX(), aOffset.getOffsetY() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffset( Position aOffset ) {
		setViewportOffset( aOffset.getPositionX(), aOffset.getPositionY() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportWidth() {
		return _viewportWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getViewportHeight() {
		return _viewportHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportWidth( int aWidth ) {
		setViewportDimension( aWidth, _viewportHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportHeight( int aHeight ) {
		setViewportDimension( _viewportWidth, aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportDimension( ViewportDimension aDimension ) {
		setViewportDimension( aDimension.getViewportWidth(), aDimension.getViewportHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportDimension( Dimension aDimension ) {
		setViewportDimension( aDimension.getWidth(), _viewportHeight = aDimension.getHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportDimension( int aWidth, int aHeight ) {
		_viewportWidth = aWidth;
		_viewportHeight = aHeight;
		resize();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldWidth() {
		return _fieldWidth;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldHeight() {
		return _fieldHeight;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getFieldGap() {
		return _fieldGap;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldWidth( int aWidth ) {
		setFieldDimension( aWidth, _fieldHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldHeight( int aHeight ) {
		setFieldHeight( aHeight );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( FieldDimension aDimension ) {
		setFieldDimension( aDimension.getFieldWidth(), aDimension.getFieldHeight() );
		setFieldGap( aDimension.getFieldGap() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( Dimension aDimension ) {
		setFieldDimension( aDimension.getWidth(), aDimension.getHeight() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public double getDragOpacity() {
		return _dragOpacity;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MoveMode getMoveMode() {
		return _moveMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GridMode getGridMode() {
		return _gridMode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setGridMode( GridMode aGridMode ) {
		if ( _gridMode != aGridMode ) {
			_gridMode = aGridMode;
			resize();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setDragOpacity( double aOpacity ) {
		_dragOpacity = aOpacity;
		if ( _dragEventHandler != null ) {
			_dragEventHandler.setDragOpacity( aOpacity );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( int aWidth, int aHeight ) {
		if ( _fieldWidth != aWidth || _fieldHeight != aHeight ) {
			_fieldWidth = aWidth;
			_fieldHeight = aHeight;
			resize();
		}
		if ( _dragEventHandler != null ) {
			_dragEventHandler.setFieldDimension( this );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldDimension( int aWidth, int aHeight, int aGap ) {
		if ( _fieldWidth != aWidth || _fieldHeight != aHeight || _fieldGap != aGap ) {
			_fieldWidth = aWidth;
			_fieldHeight = aHeight;
			_fieldGap = aGap;
			resize();
		}
		if ( _dragEventHandler != null ) {
			_dragEventHandler.setFieldDimension( this );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setFieldGap( int aGap ) {
		if ( _fieldGap != aGap ) {
			_fieldGap = aGap;
			resize();
		}
		_fieldGap = aGap;
		if ( _dragEventHandler != null ) {
			_dragEventHandler.setFieldGap( aGap );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setMoveMode( MoveMode aMoveMode ) {
		_moveMode = aMoveMode;
		if ( _dragEventHandler != null ) {
			_dragEventHandler.setMoveMode( aMoveMode );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setViewportOffset( int aOffsetX, int aOffsetY ) {
		if ( aOffsetX != _viewportOffsetX || aOffsetY != _viewportOffsetY ) {
			_viewportOffsetX = aOffsetX;
			_viewportOffsetY = aOffsetY;
			if ( _dragEventHandler != null ) {
				_dragEventHandler.setOffset( aOffsetX, aOffsetY );
			}
		}
	}

	/**
	 * Gets the move viewport duration in milliseconds.
	 *
	 * @return the move viewport duration in milliseconds
	 */
	public int getMoveViewportDurationMillis() {
		return _moveViewportDurationMillis;
	}

	/**
	 * Sets the move viewport duration in milliseconds.
	 *
	 * @param aMoveViewportDurationMillis the new move viewport duration in
	 *        milliseconds
	 */
	public void setMoveViewportDurationMillis( int aMoveViewportDurationMillis ) {
		_moveViewportDurationMillis = aMoveViewportDurationMillis;
	}

	/**
	 * With move viewport duration in milliseconds.
	 *
	 * @param aMoveViewportDurationMillis the move viewport duration in
	 *        milliseconds
	 * 
	 * @return this instance as of the builder pattern.
	 */
	public AbstractFxGridViewportPane<B> withMoveViewportDurationMillis( int aMoveViewportDurationMillis ) {
		setMoveViewportDurationMillis( aMoveViewportDurationMillis );
		return this;
	}

	/**
	 * Gets the drag viewport duration in milliseconds.
	 *
	 * @return the drag viewport duration in milliseconds
	 */
	public int getDragViewportDurationMillis() {
		return _dragViewportDurationMillis;
	}

	/**
	 * Sets the drag viewport duration in milliseconds.
	 *
	 * @param aDragViewportDurationMillis the new drag viewport duration in
	 *        milliseconds
	 */
	public void setDragViewportDurationMillis( int aDragViewportDurationMillis ) {
		_dragViewportDurationMillis = aDragViewportDurationMillis;
		if ( _dragEventHandler != null ) {
			_dragEventHandler.setDragSpriteDurationMillis( aDragViewportDurationMillis );
		}
	}

	/**
	 * With drag viewport duration in milliseconds.
	 *
	 * @param aDragViewportDurationMillis the drag viewport duration in
	 *        milliseconds
	 * 
	 * @return this instance as of the builder pattern.
	 */
	public AbstractFxGridViewportPane<B> withDragViewportDurationMillis( int aDragViewportDurationMillis ) {
		setDragViewportDurationMillis( aDragViewportDurationMillis );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Sets the listener being notified upon mouse clicks (alongside the
	 * position in the grid of the click).
	 * 
	 * @param aMouseClickedListener The listener being provided with the grid
	 *        positions of where the mouse was clicked.
	 */
	protected void onMouseClicked( BiConsumer<Integer, Integer> aMouseClickedListener ) {
		_mouseClickedListener = aMouseClickedListener;
	}

	protected Node getContent() {
		final FxGridDragSpriteEventHandler theContentEventHandler = _dragEventHandler;
		if ( theContentEventHandler != null ) {
			return theContentEventHandler.getSprite();
		}
		return null;
	}

	protected synchronized void setRoot( Node aContent ) {
		if ( _dragEventHandler != null ) {
			getChildren().remove( _dragEventHandler.getSprite() );
			_dragEventHandler.dispose();
		}
		_dragEventHandler = new FxGridDragSpriteEventHandler( aContent, this, _viewportOffsetX, _viewportOffsetY, _fieldWidth, _fieldHeight, _fieldGap, ( aOffsetX, aOffsetY ) -> { _viewportOffsetX = aOffsetX; _viewportOffsetY = aOffsetY; }, _mouseClickedListener );
		_dragEventHandler.setDragOpacity( getDragOpacity() );
		_dragEventHandler.setMoveMode( getMoveMode() );
		_dragEventHandler.setDragSpriteDurationMillis( _dragViewportDurationMillis );
		_dragEventHandler.setMoveSpriteDurationMillis( _moveViewportDurationMillis );
		if ( _hasContent && getChildren().size() != 0 ) {
			getChildren().remove( 0 );
		}
		else {
			_hasContent = true;
		}
		getChildren().add( 0, aContent );
		setWidth( aContent.getBoundsInLocal().getWidth() );
		setHeight( aContent.getBoundsInLocal().getHeight() );
	}

	/**
	 * Resizes the width and the height according to the attributes of this
	 * object.
	 */
	private void resize() {
		int theWidth = getViewportWidth() * ( getFieldWidth() + getFieldGap() );
		int theHeight = getViewportHeight() * ( getFieldHeight() + getFieldGap() );
		switch ( getGridMode() ) {
		case CLOSED -> {
			theWidth += getFieldGap();
			theHeight += getFieldGap();
		}
		case PERIODIC -> {
			theWidth -= getFieldGap();
			theHeight -= getFieldGap();
		}
		case NONE -> {
		}
		}
		setPrefSize( theWidth, theHeight );
	}
}
