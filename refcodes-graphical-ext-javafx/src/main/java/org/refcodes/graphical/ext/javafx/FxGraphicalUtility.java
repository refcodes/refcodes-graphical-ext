package org.refcodes.graphical.ext.javafx;

import static org.refcodes.textual.HorizAlignTextMode.*;
import static org.refcodes.textual.VertAlignTextMode.*;

import java.io.InputStream;

import org.refcodes.exception.BugException;
import org.refcodes.graphical.LayoutMode;
import org.refcodes.textual.HorizAlignTextMode;
import org.refcodes.textual.VertAlignTextMode;

import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * The Class FxGraphicalUtility.
 */
public final class FxGraphicalUtility {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	// @formatter:off
	private static final TextAlignModeToPos[] TEXT_ALIGN_MODE_TO_POS = new TextAlignModeToPos[] {
		new TextAlignModeToPos( LEFT, TOP, Pos.TOP_LEFT ),
		new TextAlignModeToPos( CENTER, TOP, Pos.TOP_CENTER ),
		new TextAlignModeToPos( BLOCK, TOP, Pos.TOP_CENTER ),
		new TextAlignModeToPos( RIGHT, TOP, Pos.TOP_RIGHT ),
		new TextAlignModeToPos( LEFT, MIDDLE, Pos.CENTER_LEFT ),
		new TextAlignModeToPos( CENTER, MIDDLE, Pos.CENTER),
		new TextAlignModeToPos( BLOCK, MIDDLE, Pos.CENTER ),
		new TextAlignModeToPos( RIGHT, MIDDLE, Pos.CENTER_RIGHT ),
		new TextAlignModeToPos( LEFT, BOTTOM, Pos.BOTTOM_LEFT ),
		new TextAlignModeToPos( CENTER, BOTTOM, Pos.BOTTOM_CENTER),
		new TextAlignModeToPos( BLOCK, BOTTOM, Pos.BOTTOM_CENTER),
		new TextAlignModeToPos( RIGHT, BOTTOM, Pos.BOTTOM_RIGHT )
	};
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new fx graphical utility.
	 */
	private FxGraphicalUtility() {}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * To image.
	 *
	 * @param aInputStream the input stream
	 * 
	 * @return the image
	 */
	public static Image toImage( InputStream aInputStream ) {
		return new Image( aInputStream );
	}

	/**
	 * To images.
	 *
	 * @param aInputStreams the input streams
	 * 
	 * @return the image[]
	 */
	public static Image[] toImages( InputStream... aInputStreams ) {
		final Image[] theImages = new Image[aInputStreams.length];
		for ( int i = 0; i < aInputStreams.length; i++ ) {
			theImages[i] = new Image( aInputStreams[i] );
		}
		return theImages;
	}

	/**
	 * Calculates the size for the given {@link Font} in order to fit in the
	 * given width.
	 * 
	 * @param aFontName The name of the {@link Font} to be used.
	 * @param aText The text for which to calculate the font size in order to
	 *        fit into the given width.
	 * @param aWidth The length width in which the text with the newly
	 *        calculated font-size is to fit.
	 * 
	 * @return The calculated font-size to be used for the given {@link Font} in
	 *         order for the given text to fit into the provided width.
	 */
	public static double toFontSizeWidth( String aFontName, String aText, double aWidth ) {
		final Font theFont = new Font( aFontName, 28 );
		final double theFontSize = theFont.getSize();
		final Text theText = new Text( aText );
		theText.setFont( theFont );
		final double theWidth = theText.getBoundsInLocal().getWidth();
		final double theFitSize = theFontSize * aWidth / theWidth;
		return theFitSize;
	}

	/**
	 * Calculates the size for the given {@link Font} in order to fit in the
	 * given height.
	 * 
	 * @param aFontName The name of the {@link Font} to be used.
	 * @param aText The text for which to calculate the font size in order to
	 *        fit into the given height.
	 * @param aHeight The height in which the text with the newly calculated
	 *        font-size is to fit.
	 * 
	 * @return The calculated font-size to be used for the given {@link Font} in
	 *         order for the given text to fit into the provided height.
	 */
	public static double toFontSizeHeight( String aFontName, String aText, double aHeight ) {
		final Font theFont = new Font( aFontName, 28 );
		final double theFontSize = theFont.getSize();
		final Text theText = new Text( aText );
		theText.setFont( theFont );
		final double theHeight = theText.getBoundsInLocal().getHeight();
		final double theFitSize = theFontSize * aHeight / theHeight;
		return theFitSize;
	}

	/**
	 * Calculates the size for the given {@link Font} in order to fit in the
	 * given length (width or height).
	 * 
	 * @param aFontName The name of the {@link Font} to be used.
	 * @param aText The text for which to calculate the font size in order to
	 *        fit into the given length (width or height).
	 * @param aLength The length (width or height, as of the {@link LayoutMode})
	 *        in which the text with the newly calculated font-size is to fit.
	 * @param aLayoutMode The mode to determine whether to use the width or the
	 *        height to calculate relative to.
	 * 
	 * @return The calculated font-size to be used for the given {@link Font} in
	 *         order for the given text to fit into the provided length (width
	 *         or height).
	 */
	public static double toFontSize( String aFontName, String aText, double aLength, LayoutMode aLayoutMode ) {
		return switch ( aLayoutMode ) {
		case HORIZONTAL -> toFontSizeWidth( aFontName, aText, aLength );
		case VERTICAL -> toFontSizeHeight( aFontName, aText, aLength );
		};
	}

	/**
	 * Calculates the size for the given {@link Font} in order to fit in the
	 * given length (width or height) and returns an accordingly calculated
	 * {@link Font} with the same {@link Font#getName()}.
	 * 
	 * @param aFontName The name of the {@link Font} to be used.
	 * @param aText The text for which to calculate the font size in order to
	 *        fit into the given length (width or height).
	 * @param aLength The length (width or height, as of the {@link LayoutMode})
	 *        in which the text with the newly calculated font-size is to fit.
	 * @param aLayoutMode The mode to determine whether to use the width or the
	 *        height to calculate relative to.
	 * 
	 * @return The calculated {@link Font} to be used for the given {@link Font}
	 *         in order for the given text to fit into the provided length
	 *         (width or height).
	 */
	public static Font toFont( String aFontName, String aText, double aLength, LayoutMode aLayoutMode ) {
		final double theFontSize = toFontSize( aFontName, aText, aLength, aLayoutMode );
		final Font theFont = new Font( aFontName, theFontSize );
		return theFont;
	}

	/**
	 * Calculates the size for the given {@link Font} in order to fit in the
	 * given width and returns an accordingly calculated {@link Font} with the
	 * same {@link Font#getName()}.
	 * 
	 * @param aFontName The name of the {@link Font} to be used.
	 * @param aText The text for which to calculate the font size in order to
	 *        fit into the given width.
	 * @param aWidth The width in which the text with the newly calculated
	 *        font-size is to fit.
	 * 
	 * @return The calculated {@link Font} to be used for the given {@link Font}
	 *         in order for the given text to fit into the provided width.
	 */
	public static Font toFontWidth( String aFontName, String aText, double aWidth ) {
		return toFont( aFontName, aText, aWidth, LayoutMode.HORIZONTAL );
	}

	/**
	 * Calculates the size for the given {@link Font} in order to fit in the
	 * given height and returns an accordingly calculated {@link Font} with the
	 * same {@link Font#getName()}.
	 * 
	 * @param aFontName The name of the {@link Font} to be used.
	 * @param aText The text for which to calculate the font size in order to
	 *        fit into the given height.
	 * @param aHeight The height in which the text with the newly calculated
	 *        font-size is to fit.
	 * 
	 * @return The calculated {@link Font} to be used for the given {@link Font}
	 *         in order for the given text to fit into the provided height.
	 */
	public static Font toFontHeight( String aFontName, String aText, double aHeight ) {
		return toFont( aFontName, aText, aHeight, LayoutMode.VERTICAL );
	}

	/**
	 * Converts them text align modes to a {@link Pos} element.
	 * 
	 * @param aHorizAlignTextMode The horizontal portion to determine the
	 *        according {@link Pos} element.
	 * @param aVertAlignTextMode The vertical portion to determine the according
	 *        {@link Pos} element.
	 *
	 * @return The accordingly determines {@link Pos} element.
	 */
	public static Pos toPos( HorizAlignTextMode aHorizAlignTextMode, VertAlignTextMode aVertAlignTextMode ) {
		for ( TextAlignModeToPos ePos : TEXT_ALIGN_MODE_TO_POS ) {
			if ( ePos.vertAlignTextMode == aVertAlignTextMode && ePos.horizAlignTextMode == aHorizAlignTextMode ) {
				return ePos.pos;
			}
		}
		throw new BugException( "Missing <Pos> definition for combination <" + aHorizAlignTextMode + "> and <" + aVertAlignTextMode + "> in implementation!" );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private static class TextAlignModeToPos {

		HorizAlignTextMode horizAlignTextMode;
		VertAlignTextMode vertAlignTextMode;
		Pos pos;

		public TextAlignModeToPos( HorizAlignTextMode aHorizAlignTextMode, VertAlignTextMode aVertAlignTextMode, Pos aPos ) {
			horizAlignTextMode = aHorizAlignTextMode;
			vertAlignTextMode = aVertAlignTextMode;
			pos = aPos;
		}
	}
}
