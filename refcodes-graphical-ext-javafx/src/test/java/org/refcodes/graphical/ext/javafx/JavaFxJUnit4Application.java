// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical.ext.javafx;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * This is the application which starts JavaFx. It is controlled through the
 * startJavaFx() method.
 * http://awhite.blogspot.de/2013/04/javafx-junit-testing.html
 */
public class JavaFxJUnit4Application extends Application {

	private static final ReentrantLock LOCK = new ReentrantLock();

	private static final AtomicBoolean started = new AtomicBoolean();

	/**
	 * Start JavaFx.
	 */
	public static void startJavaFx() {
		try {
			// Lock or wait.  This gives another call to this method time to finish
			// and release the lock before another one has a go
			LOCK.lock();

			if ( !started.get() ) {
				// start the JavaFX application
				final ExecutorService executor = Executors.newSingleThreadExecutor();
				executor.execute( () -> JavaFxJUnit4Application.launch() );

				while ( !started.get() ) {
					Thread.yield();
				}
			}
		}
		finally {
			LOCK.unlock();
		}
	}

	/**
	 * Launch.
	 */
	protected static void launch() {
		Application.launch();
	}

	/**
	 * An empty start method.
	 *
	 * @param stage The stage
	 */
	@Override
	public void start( final Stage stage ) {
		started.set( Boolean.TRUE );
	}
}