// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical.ext.javafx;

import org.refcodes.graphical.GridMode;
import org.refcodes.graphical.MoveMode;
import org.refcodes.graphical.RasterPropertyBuilder;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ViewportRasterPanelTest extends Application {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public void start( Stage aPrimaryStage ) {
		final RasterPropertyBuilder theRaster = new RasterPropertyBuilder();
		theRaster.withGridDimension( 16, 16 ).withFieldDimension( 50, 50 ).withFieldGap( 1 ).withGridMode( GridMode.CLOSED );
		final FxRasterFactory theRasterFactory = new FxRasterFactory();
		final Pane theBackground = theRasterFactory.create( theRaster );
		final FxGridViewportPane theViewportPanel = new FxGridViewportPane();
		theViewportPanel.setRoot( theBackground );
		theViewportPanel.setFieldDimension( theRaster );
		theViewportPanel.setMoveMode( MoveMode.SMOOTH );
		theRaster.withGridDimension( 2, 2 );
		final Node theRectangle = theRasterFactory.withOddFieldColor( Color.RED ).withEvenFieldColor( Color.BLUE ).create( theRaster );
		final FxGridDragSpriteEventHandler theDragRectEventHandler = new FxGridDragSpriteEventHandler( theRectangle, theBackground );
		theDragRectEventHandler.setFieldDimension( theRaster );
		theDragRectEventHandler.setMoveMode( MoveMode.SMOOTH );
		theBackground.getChildren().add( theRectangle );
		aPrimaryStage.setTitle( getClass().getSimpleName() );
		final Scene theScene = new Scene( theViewportPanel );
		final double theDecorationH = aPrimaryStage.getWidth() - theScene.getWidth();
		final double theDecorationV = aPrimaryStage.getHeight() - theScene.getHeight();
		aPrimaryStage.setMaxWidth( theViewportPanel.getMaxWidth() + theDecorationH );
		aPrimaryStage.setMaxHeight( theViewportPanel.getMaxHeight() + theDecorationV );
		aPrimaryStage.setScene( theScene );
		aPrimaryStage.show();
		// theViewportPanel.setViewportOffset( -5, -5 );
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// MAIN:

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Usually ignored, only used if JavaFx can not not be started with
	 * start(Stage).
	 *
	 * @param args the arguments
	 */
	public static void main( String[] args ) {
		launch( args );
	}
}
