// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.graphical.ext.javafx;

import java.util.concurrent.CountDownLatch;

import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

import javafx.application.Platform;

/**
 * This basic class runner ensures that JavaFx is running and then wraps all the
 * runChild() calls in a Platform.runLater(). runChild() is called for each test
 * that is run. By wrapping each call in the Platform.runLater() this ensures
 * that the request is executed on the JavaFx thread.
 * http://awhite.blogspot.de/2013/04/javafx-junit-testing.html
 */
public class JavaFxJUnit4ClassRunner extends BlockJUnit4ClassRunner {
	/**
	 * Constructs a new JavaFxJUnit4ClassRunner with the given parameters.
	 * 
	 * @param clazz The class that is to be run with this Runner
	 * 
	 * @throws InitializationError Thrown by the BlockJUnit4ClassRunner in the
	 *         super()
	 */
	public JavaFxJUnit4ClassRunner( final Class<?> clazz ) throws InitializationError {
		super( clazz );

		JavaFxJUnit4Application.startJavaFx();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void runChild( final FrameworkMethod method, final RunNotifier notifier ) {
		// Create a latch which is only removed after the super runChild()
		// method has been implemented.
		final CountDownLatch latch = new CountDownLatch( 1 );
		Platform.runLater( () -> {
			// Call super to actually do the work
			JavaFxJUnit4ClassRunner.super.runChild( method, notifier );

			// Decrement the latch which will now proceed.
			latch.countDown();
		} );
		try {
			latch.await();
		}
		catch ( InterruptedException e ) {
			// Waiting for the latch was interruped
			e.printStackTrace();
		}
	}
}